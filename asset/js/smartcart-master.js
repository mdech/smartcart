
$(document).ready(function($) {
	$( "body" ).delegate( ".navbar-bottom .list", "click touchstart", function() {
	    $(this).siblings().removeClass('selected');
	    $(this).addClass('selected');
	});

	$(".loyalty-card .flip-container").on("click touchstart", function() {
		$(this).toggleClass("active");
	});

	// loyalty detail js //

	$( "body" ).delegate( ".loyalty-detail-2 .pull-row .col-4", "click touchstart", function() {
	    $(this).siblings().removeClass('active');
	    $(this).addClass('active');
	});

	$( "body" ).delegate( ".loyalty-detail-2 .pull-row .col-6", "click touchstart", function() {
	    $(this).siblings().removeClass('active');
	    $(this).addClass('active');
	});

	// $(".loyalty-detail-3 .pull-row .coupon-tab").on("click touchstart", function() {
	//     $('.loyalty-detail-3 .slide').removeClass('slide-active')
	// });
	// $(".loyalty-detail-3 .pull-row .exclusive-tab").on("click touchstart", function() {
	//     $('.loyalty-detail-3 .slide').addClass('slide-active')
	// });
	// $(".loyalty-detail-3 .pull-row .third-tab").on("click touchstart", function() {
	//     $('.loyalty-detail-3 .slide').addClass('active-third')
	// });

	$('.loyalty-detail-2 .pull-row .coupon-tab').click(function() {
		$('.tab.coupon').addClass('active').siblings().removeClass('active');
		// $('.tab').siblings().removeClass('active');
	})
	$('.loyalty-detail-2 .pull-row .exclusive-tab').click(function() {
		$('.tab.exclusive').addClass('active').siblings().removeClass('active');
		// $('.tab').siblings().removeClass('active');
	})
	$('.loyalty-detail-2 .pull-row .third-tab').click(function() {
		$('.tab.third').addClass('active').siblings().removeClass('active');
		// $('.tab').siblings().removeClass('active');
	})


	// $('.loyalty-detail-3 .pull-row .coupon-tab').click(function() {
	// 	$()
	// })

});

$(window).scroll(function() {    
	 
    var scroll = $(window).scrollTop();

    if (scroll >= 50) {
        $(".navbar-top").addClass("active");
        $(".history-detail-1").addClass("active");
        // $(".history-detail-1").removeClass("first-row");
    } else {
        $(".navbar-top").removeClass("active");
        $(".history-detail-1").removeClass("active");
        // $(".history-detail-1").addClass("first-row");
    }
});

$(document).ready(function() {
	$('.payment-method .col-6 .card.ovo').click(function() {
		if ($(this).hasClass('active') == true) {
			$('.payment-method .col-6 .card').removeClass('active');
			$('.pm').text("");
		} else {
			$(this).addClass('active').siblings().removeClass('active');
			$('.pm').text("you choose OVO as payment method");
		}
	});
	$('.payment-method .col-6 .card.gojek').click(function() {
		if ($(this).hasClass('active') == true) {
			$('.payment-method .col-6 .card').removeClass('active');
			$('.pm').text("");
		} else {
			$(this).addClass('active').siblings().removeClass('active');
			$('.pm').text("you choose GOJEK as payment method");
		}
	});
	// $('.payment-method .card.gojek').click(function() {
	// 	$(this).addClass('active').siblings().removeClass('active');
	// })

	// $('.payment-method .card.ovo').click(function() {
	// 	$(this).addClass('active').siblings().removeClass('active');
	// })

})