<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>SmartCart Mockup</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="shortcut icon" type="image" href="asset/images/gaspace-icon.png">
		<link rel="stylesheet" type="text/css" href="asset/css/credit-score.css" media="all"/>
		<link rel="stylesheet" type="text/css" href="asset/css/animation.css" media="all"/>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
		<script src="asset/js/jquery-2.1.1.js"></script>
		<script src="asset/js/jquery-ui.js"></script>
		<script src="asset/js/smartcart-master.js"></script>
		
	</head>
	<body>
		<div class="master-body">
			<div class="navbar-top">
				<div class="center-logo"></div>
				<div class="right-frame">
					<i class="fas fa-user"></i>
				</div>
			</div>
			<div class="navbar-bottom row">
				<div class="col-3 list" onclick="location.href='index.php';">
					<div class="icon"></div>
					<div class="label">
						Home
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list" onclick="location.href='promo-index.php';">
					<div class="icon"></div>
					<div class="label">
						Promo
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list" onclick="location.href='nearby-index.php';">
					<div class="icon"></div>
					<div class="label">
						Nearby
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list selected">
					<div class="icon"></div>
					<div class="label">
						Target
					</div>	
					<span class="clear"></span>
				</div>
			</div>

			<!-- S T A R T   C U S T O M I Z A T I O N   F R O M   H E R E -->

			<div class="header sub-menu"> <!-- Header -->
				<div class="p-18">
					<!-- <div class="bottom-image"></div> -->
				</div>
			</div>
			<div class="p-18">
				<div class="row row-cs-1">
					<div class="col-12">
						<h4 class="display-4"><b>Credit Score</b> Target</h4>
					</div>
				</div>	
				<div class="row row-cs-2">
					<div class="col-12">	
						<div class="card">
							<div class="p-15">
								<div class="graph-emoticon-frame stat-5">
									<div class="title-top">
										Salary Expenditure Charts
									</div>
									<div class="content">
										<div class="graph">
											<div class="body stat-1"></div>
											<div class="body stat-2"></div>
											<div class="body stat-3"></div>
											<div class="body stat-4"></div>
											<div class="body stat-5"></div>
											<div class="body-active">
												<div class="line" style="width:85%">
													<img src="asset/images/icon-emoticon-5.png">
												</div>
											</div>
										</div>	
										<div class="numbers">
											<div class="title">457</div>
											<div class="desc">Excellent</div>
										</div>
										<br class="clear">
									</div>
								</div>
							</div>
							<div class="p-15">
								<div class="graph-emoticon-frame stat-4">
									<div class="title-top">
										Public Utilities Payment
									</div>
									<div class="content">
										<div class="graph">
											<div class="body stat-1"></div>
											<div class="body stat-2"></div>
											<div class="body stat-3"></div>
											<div class="body stat-4"></div>
											<div class="body stat-5"></div>
											<div class="body-active">
												<div class="line" style="width:70%">
													<img src="asset/images/icon-emoticon-4.png">
												</div>
											</div>
										</div>	
										<div class="numbers">
											<div class="title">391</div>
											<div class="desc">Great</div>
										</div>
										<br class="clear">
									</div>
								</div>
							</div>
							<div class="p-15">
								<div class="graph-emoticon-frame stat-3">
									<div class="title-top">
										Automatic Payments Settings
									</div>
									<div class="content">
										<div class="graph">
											<div class="body stat-1"></div>
											<div class="body stat-2"></div>
											<div class="body stat-3"></div>
											<div class="body stat-4"></div>
											<div class="body stat-5"></div>
											<div class="body-active">
												<div class="line" style="width:50%">
													<img src="asset/images/icon-emoticon-3.png">
												</div>
											</div>
										</div>	
										<div class="numbers">
											<div class="title">208</div>
											<div class="desc">Need Work</div>
										</div>
										<br class="clear">
									</div>
								</div>
							</div>
						</div>	
					</div>
				</div>
			</div>
			<div class="divider-title">
				<!-- <i class="fas fa-ellipsis-h right"></i> -->
				<div class="title">
					<h5 class="display-5"><b>Expenses</b> Graphs</h5>
				</div>
			</div>			
			<div class="p-18">
				<div class="row row-cs-3">
					<div class="col-12">
						<div class="card">
							<div class="p-15">
								<div class="graph-emoticon-frame stat-4">
									<div class="title-top">
										Monthly Transport Expenses
									</div>
									<div class="content">
										<div class="graph">
											<div class="body stat-1"></div>
											<div class="body stat-2"></div>
											<div class="body stat-3"></div>
											<div class="body stat-4"></div>
											<div class="body stat-5"></div>
											<div class="body-active">
												<div class="line" style="width:75%">
													<img src="asset/images/icon-emoticon-4.png">
												</div>
											</div>
										</div>	
										<div class="numbers">
											<div class="title">391</div>
											<div class="desc">Great</div>
										</div>
										<br class="clear">
									</div>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="p-15">
								<div class="graph-emoticon-frame stat-2">
									<div class="title-top">
										Monthly Digital Expenses
									</div>
									<div class="content">
										<div class="graph">
											<div class="body stat-1"></div>
											<div class="body stat-2"></div>
											<div class="body stat-3"></div>
											<div class="body stat-4"></div>
											<div class="body stat-5"></div>
											<div class="body-active">
												<div class="line" style="width:35%">
													<img src="asset/images/icon-emoticon-2.png">
												</div>
											</div>
										</div>	
										<div class="numbers">
											<div class="title">150</div>
											<div class="desc">Not Good</div>
										</div>
										<br class="clear">
									</div>
								</div>
							</div>
						</div>
					</div>	
				</div>		
			</div>	
			<div class="footer-frame"></div>
		</div><!-- master body -->
	</body>
</html>

