
<?php
	$conn = mysqli_connect("localhost", "root", "", "tilltalk_demo");

	if (!$conn) {
		echo "Error: Unable to connect to MySQL." . PHP_EOL;
		echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
		echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
		exit;
	}
?>
<?php 
	$id 	= $_GET['id'];
	$sql 	= "SELECT `tr_shop_detail`.* , store_name, store_location, ms_product.img, ms_product.recipe
				FROM `tr_shop_detail`
				JOIN `tr_shop_master` ON `tr_shop_master`.`id` = `tr_shop_detail`.`id_master`
				JOIN `ms_cashier` ON `ms_cashier`.`id` = `tr_shop_master`.`id_kasir`
				JOIN `ms_store` ON `ms_store`.`id` = `ms_cashier`.`id_store`
				JOIN `ms_product` ON `ms_product`.`name` = `tr_shop_detail`.`product_name`
				WHERE `id_master` = $id
				ORDER BY `id_master`  DESC";

	$discount 	= rand(10, 50);
	$result 	= $conn->query($sql);


	
	if ($result->num_rows > 0) {
		// output data of each row
		while($row = $result->fetch_assoc()) {
			$__detail.= '  <tr class="popup-tg" data-id="'.$row['id'].'"  data-name="'.$row['product_name'].'"  data-recipe="'.$row['recipe'].'"  data-img="'.$row['img'].'">
								<td>'.$row['product_name'].'</td>
								<td>'.$row['qty'].'</td>
								<td>'.number_format($row['price']).'</td>
							</tr>';

			$location 	 = $row['store_name'];
			$date 	 	 = $row['entry_stamp'];
			$subtotal 	 = $row['qty'] * $row['price'];
			$total		+= $subtotal; 
		}
	} else {
		$__detail =  "<tr><td colspan=3 style='text-align:center !important;'>No Data</td></tr>";
	}

	$save 	= /*$total -*/ $total * ($discount/100);
	$point	= $total * 0.001;
?>

<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>SmartCart Mockup</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="shortcut icon" type="image" href="asset/images/gaspace-icon.png">
		<link rel="stylesheet" type="text/css" href="asset/css/history-detail.css" media="all"/>
		<link rel="stylesheet" type="text/css" href="asset/css/animation.css" media="all"/>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
		<script src="asset/js/jquery-2.1.1.js"></script>
		<script src="asset/js/jquery-ui.js"></script>
		<script src="asset/js/smartcart-master.js"></script>
		
	</head>
	<body>
		<div class="master-body">
			<div class="navbar-top">
				<div class="back-button" onclick="location.href='history-index.php'">
					<i class="fas fa-chevron-left"></i>
				</div>
			</div>
			<div class="navbar-bottom row">
				<div class="col-3 list" onclick="location.href='index.php';">
					<div class="icon"></div>
					<div class="label">
						Home
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list" onclick="location.href='promo-index.php';">
					<div class="icon"></div>
					<div class="label">
						Promo
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list" onclick="location.href='nearby-index.php';">
					<div class="icon"></div>
					<div class="label">
						Nearby
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list" onclick="location.href='credit-score.php';">
					<div class="icon"></div>
					<div class="label">
						Target
					</div>	
					<span class="clear"></span>
				</div>
			</div>

			<!-- S T A R T   C U S T O M I Z A T I O N   F R O M   H E R E -->

			<div class="header index history-detail"> <!-- Header -->
				<div class="p-18">
					<div class="bottom-image"></div>
				</div>
			</div>

			<div class="p-18"> <!-- body -->	
				<div class="row first-row history-detail-1">
					<div class="col-12">
						<div class="detail-header">
							<div class="p-18">
								<h6><span>ID</span> 000<?php echo $id?></h6>
								<h3><?php echo $location?></h3>
								<h5><?php echo date_format(date_create($date),"d M Y H:i");?></h5>
							</div>
						</div>
					</div>
				</div>
				<div class="row history-detail-2">
					<div class="col-12">
						<div class="card history-header">
							<div class="p-18">
								<div class="left">
									<div class="title">
										<h5>Spend In <i class="fas fa-caret-down down"></i></h5>
										<h4>Rp. <?php echo number_format($total);?></h4>
									</div>
									<img src="asset/images/icon-transaction.png" alt="" class="icon">
								</div>
								<div class="middle">
									<img src="asset/images/icon-return.png" alt="">
								</div>
								<div class="right">
									<div class="title">
										<h5>Saving <i class="fas fa-caret-up up"></i></h5>
										<h4>Rp. <?php echo number_format($save);?></h4>
									</div>
									<img src="asset/images/icon-saving.png" alt="" class="icon">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row history-detail-3">
					<div class="col-12">
					
						<div class="card">
							<div class="p-18">
								<div class="search">
									<i class="fas fa-search"></i>
									<input type="text" placeholder="Search here">
								</div>
								<table class="receipt-table">
									<thead>
										<tr>
											<th>Item</th>
											<th width="40">Qty</th>
											<th width="60">Price</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											#FETCH DETAIL ROW OF TRANSACTION
											echo $__detail;
										?>
									</tbody>
								</table>
								<table class="receipt-footer"><!-- receipt footer -->
									<tbody class="receipt-footer">
										<tr>
											<th>Total Transaction</th>
											<th>:</th>
											<th><?php echo number_format($total);?></th>
										</tr>
										<tr>
											<th>Discount</th>
											<th>:</th>
											<th><?php echo $discount."% (-".number_format($save).")";?></th>
										</tr>
										<tr>
											<th>Point Earn</th>
											<th>:</th>
											<th><img src="asset/images/icon-dollar.png" alt=""><?php echo number_format($point);?></th>
										</tr>
									</tbody>
								</table>
							</div>

						</div>
					</div>
				</div>

				<!-- popup image : popup-bg-oreo.jpg, popup-bg-tea.jpg, popup-bg-instant-noodles.jpg, popup-bg-coklat.jpg, popup-bg-corned-beef.jpeg; -->
				
				<div id="bg__"></div>
				
			</div>
			
			<div class="footer-frame"></div>
		</div><!-- master body -->
	</body>

	<script>
		$(document).ready(function() {
			$('.right-frame').click(function() {
				$('.filter').addClass('active');
			});
			$('.button-close').click(function() {
				$('.filter').removeClass('active');
			});

		// POPUP
		$('.popup-tg').click(function() {
			console.log($(this).data("id"));
			img 	= "url('asset/images/"+$(this).data('img')+"')";
			name 	= $(this).data("name");
			recipe 	= "location.href='"+$(this).data("recipe")+"'";
			
			$("#bg__").empty();
			$("#bg__").append('<div class="popup history-detail" style="background-image:'+img+';"><div class="overlay"></div><div class="p-18"><div class="button-close"><i class="fas fa-times"></i></div><div class="popup-header"><h2 id="__name">'+name+'</h2></div><div class="popup-body"></div><div class="popup-footer"><button><!-- <i class="fas fa-shopping-cart"></i> --><img src="asset/images/icon-shopping.png" alt="" class="icon"><b>Buy</b></button><button onclick="'+recipe+'"><img src="asset/images/icon-recipe.png" alt="" class="icon"><b>Recipe</b></button></div></div></div>');
			$('.popup').addClass('active');
			$('.button-close').click(function() {
				$('.popup').removeClass('active');
			});
		});

		})
	</script>
</html>