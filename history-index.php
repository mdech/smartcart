<?php
	$conn = mysqli_connect("localhost", "root", "", "tilltalk_demo");

	if (!$conn) {
		echo "Error: Unable to connect to MySQL." . PHP_EOL;
		echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
		echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
		exit;
	}


	$id 	= 1;
	$sql 	= "SELECT `tr_shop_master`.* ,store_name, store_location
                    ,(SELECT SUM(`qty` * price) FROM tr_shop_detail WHERE id_master = tr_shop_master.id) as total
				FROM `tr_shop_master`
				LEFT JOIN `ms_cashier` ON `ms_cashier`.`id` = `tr_shop_master`.`id_kasir`
				LEFT JOIN `ms_store` ON `ms_store`.`id` = `ms_cashier`.`id_store`
				-- WHERE  `tr_shop_master`.`entry_stamp` = MONTH(2)
				WHERE `timestamp` BETWEEN '2019-03-01 00:00:00.000000' AND '2019-03-31 00:00:00.000000'

				-- GROUP BY to_char(entry_stamp, 'YYYY-MM')
				ORDER BY `id`  DESC";
	$discount 	= rand(10, 50);
	$result 	= $conn->query($sql);

    
	if ($result->num_rows > 0) {
		// output data of each row
		while($row = $result->fetch_assoc()) {
            $__link   = "'history-detail.php?id=".$row['id']."'";
			$__detail.= '  <div class="card" onclick="location.href='.$__link.'">
							<div class="p-18">
								<div class="left">
									<h5>
										<i class="fas fa-store"></i>
										<span class="text">'.$row['store_name'].'</span>
									</h5>
									<h6>
										<span class="text"><span>'.date_format(date_create($row['timestamp']),"d M ").' </span> '.date_format(date_create($row['timestamp']),"Y, H.i").'</span>
									</h6>
								</div>
								<div class="right">
									<h4><sup>Rp</sup>'.number_format($row['total']).'</h4>
									<h6><span>ID</span> 000'.$row['id'].'</h6>
								</div>
							</div>
						</div>';

			$location 	 = $row['store_name'];
			$date 	 	 = $row['entry_stamp'];
			$subtotal 	 = $row['qty'] * $row['price'];
			$total		+= $subtotal; 
		}
	} else {
		$__detail =  "<tr><td colspan=3 style='text-align:center !important;'>No Data</td></tr>";
	}
?>

<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>SmartCart Mockup</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="shortcut icon" type="image" href="asset/images/gaspace-icon.png">
		<link rel="stylesheet" type="text/css" href="asset/css/history-index.css" media="all"/>
		<link rel="stylesheet" type="text/css" href="asset/css/animation.css" media="all"/>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
		<script src="asset/js/jquery-2.1.1.js"></script>
		<script src="asset/js/jquery-ui.js"></script>
		<script src="asset/js/smartcart-master.js"></script>
		
	</head>
	<body>
		<div class="master-body">
			<div class="navbar-top">
				<div class="back-button" onclick="location.href='index.php'">
					<i class="fas fa-chevron-left"></i>
				</div>
				<div class="left-frame">
					<div class="search">
						<div class="icon search">
							<i class="fas fa-search"></i>
						</div>
						<input type="text">
					</div>
				</div>
				<div class="right-frame">
					<i class="fas fa-filter"></i>
				</div>
			</div>
			<div class="navbar-bottom row">
				<div class="col-3 list" onclick="location.href='index.php';">
					<div class="icon"></div>
					<div class="label">
						Home
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list" onclick="location.href='promo-index.php';">
					<div class="icon"></div>
					<div class="label">
						Promo
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list" onclick="location.href='nearby-index.php';">
					<div class="icon"></div>
					<div class="label">
						Nearby
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list" onclick="location.href='credit-score.php';">
					<div class="icon"></div>
					<div class="label">
						Target
					</div>	
					<span class="clear"></span>
				</div>
			</div>

			<!-- S T A R T   C U S T O M I Z A T I O N   F R O M   H E R E -->

			<div class="header index history-index"> <!-- Header -->
				<div class="p-18">
					<div class="bottom-image"></div>
				</div>
			</div>

			<div class="p-18"> <!-- body -->	
				<div class="row first-row history-index-1">
					<div class="col-12">
						<div class="card history-header">
							<div class="p-18">
								<div class="left">
									<div class="title">
										<h5>Total Transaction <i class="fas fa-caret-down down"></i></h5>
										<h4>Rp. 2.288.000</h4>
									</div>
									<span class="sub-title">
										<i class="fas fa-calendar-alt"></i>
										<h6>Jan - Feb, 2019</h6>
									</span>
									<img src="asset/images/icon-transaction.png" alt="" class="icon">
								</div>
								<div class="middle">
									<img src="asset/images/icon-return.png" alt="">
								</div>
								<div class="right">
									<div class="title">
										<h5>Saving <i class="fas fa-caret-up up"></i></h5>
										<h4>Rp. 548.000</h4>
									</div>
									<span class="sub-title">
										<i class="fas fa-calendar-alt"></i>
										<h6>Jan - Feb, 2019</h6>
									</span>
									<img src="asset/images/icon-saving.png" alt="" class="icon">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- month -->
			<div class="history-index-2">
				<div class="p-18">
					<i class="fas fa-calendar-alt"></i>
					<span class="text"><span>Maret</span> 2019</span>
				</div>
			</div>
			<!-- month end -->

			<div class="p-18">
				<div class="row history-index-3">
					<div class="col-12">
						<!-- FETCH DETAIL ROW -->
						<?php echo $__detail;?>
					</div>
				</div> <!-- body end -->
			</div>	<!-- p18 end -->

			<!-- month -->
			<div class="history-index-2">
				<div class="p-18">
					<i class="fas fa-calendar-alt"></i>
					<span class="text"><span>February</span> 2019</span>
				</div>
			</div>
			<!-- month end -->

			<div class="p-18">
				<div class="row history-index-3">
					<div class="col-12">
						<div class="card" onclick="location.href='history-detail.php'">
							<div class="p-18">
								<div class="left">
									<h5>
										<i class="fas fa-store"></i>
										<span class="text">Kota Kasablanka</span>
									</h5>
									<h6>
										<span class="text"><span>24 February</span> 2019, 14.30</span>
									</h6>
								</div>
								<div class="right">
									<h4><sup>Rp</sup>280.500</h4>
									<h6><span>ID</span> 000944</h6>
								</div>
							</div>
						</div>
						<div class="card" onclick="location.href='history-detail.php'">
							<div class="p-18">
								<div class="left">
									<h5>
										<i class="fas fa-store"></i>
										<span class="text">Cibinong City Mall</span>
									</h5>
									<h6>
										<span class="text"><span>18 February</span> 2019, 13.20</span>
									</h6>
								</div>
								<div class="right">
									<h4><sup>Rp</sup>382.000</h4>
									<h6><span>ID</span> 000943</h6>
								</div>
							</div>
						</div>
					</div>
				</div> <!-- body end -->
			</div>	<!-- p18 end -->

			<!-- month -->
			<div class="history-index-2">
				<div class="p-18">
					<i class="fas fa-calendar-alt"></i>
					<span class="text"><span>January</span> 2019</span>
				</div>
			</div>
			<!-- month end -->

			<div class="p-18">
				<div class="row history-index-3">
					<div class="col-12">	
						<div class="card" onclick="location.href='history-detail.php'">
							<div class="p-18">
								<div class="left">
									<h5>
										<i class="fas fa-store"></i>
										<span class="text">Botani Square</span>
									</h5>
									<h6>
										<span class="text"><span>12 January</span> 2019, 10.30</span>
									</h6>
								</div>
								<div class="right">
									<h4><sup>Rp</sup>182.000</h4>
									<h6><span>ID</span> 000942</h6>
								</div>
							</div>
						</div>
						<div class="card" onclick="location.href='history-detail.php'">
							<div class="p-18">
								<div class="left">
									<h5>
										<i class="fas fa-store"></i>
										<span class="text">Grand Indonesia</span>
									</h5>
									<h6>
										<span class="text"><span>10 April</span> 2019, 17.00</span>
									</h6>
								</div>
								<div class="right">
									<h4><sup>Rp</sup>882.000</h4>
									<h6><span>ID</span> 000941</h6>
								</div>
							</div>
						</div>
						<div class="card" onclick="location.href='history-detail.php'">
							<div class="p-18">
								<div class="left">
									<h5>
										<i class="fas fa-store"></i>
										<span class="text">Senayan City</span>
									</h5>
									<h6>
										<span class="text"><span>4 January</span> 2019, 19.30</span>
									</h6>
								</div>
								<div class="right">
									<h4><sup>Rp</sup>562.000</h4>
									<h6><span>ID</span> 000940</h6>
								</div>
							</div>
						</div>
					</div>
				</div> <!-- body end -->
			</div>	<!-- p18 end -->

			<div class="filter">
				<div class="p-18">
					<div class="button-close">
						<i class="fas fa-times"></i>
					</div>
					<div class="filter-header">
						<h3>Sort by</h3>
					</div>
					<div class="filter-body">
						<div class="start-input">
							<label for="">From</label>
							<br>
							<div class="input">
								<i class="fas fa-calendar-alt"></i>
								<input type="date">
							</div>
						</div>
						<div class="end-input">
							<label for="">To</label>
							<br>
							<div class="input">
								<i class="fas fa-calendar-alt"></i>
								<input type="date">
							</div>
						</div>
					</div>
					<div class="filter-footer">
						<button>Apply</button>
					</div>
				</div>
			</div>
			
			<div class="footer-frame"></div>
		</div><!-- master body -->
	</body>

	<script>
		$(document).ready(function() {
			$('.right-frame').click(function() {
				$('.filter').addClass('active');
			});
			$('.button-close').click(function() {
				$('.filter').removeClass('active');
			});
		})
	</script>	
</html>

