<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>SmartCart Mockup</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="shortcut icon" type="image" href="asset/images/gaspace-icon.png">
		<link rel="stylesheet" type="text/css" href="asset/css/style.css" media="all"/>
		<link rel="stylesheet" type="text/css" href="asset/css/animation.css" media="all"/>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
		<script src="asset/js/jquery-2.1.1.js"></script>
		<script src="asset/js/jquery-ui.js"></script>
		<script src="asset/js/smartcart-master.js"></script>
		
	</head>
	<body>
		<div class="master-body">
			<div class="navbar-top">
				<div class="right-frame">
					<i class="fas fa-user"></i>
				</div>
			</div>
			<div class="navbar-bottom row">
				<div class="col-3 list selected">
					<div class="icon"></div>
					<div class="label">
						Home
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list" onclick="location.href='promo-index.php';">
					<div class="icon"></div>
					<div class="label">
						Promo
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list" onclick="location.href='nearby-index.php';">
					<div class="icon"></div>
					<div class="label">
						Nearby
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list" onclick="location.href='credit-score.php';">
					<div class="icon"></div>
					<div class="label">
						Target
					</div>	
					<span class="clear"></span>
				</div>
			</div>
			<div class="header index">
				<div class="p-18">
					<div class="bottom-image"></div>
				</div>
			</div>
			<div class="p-18" style="display: none;">
				<div class="row first-row loyalty-card">
					<div class="col-12">
						<div class="loyalty-card">
							<div class="flip-container">
								<div class="flipper">
									<div class="front">
							      		<div class="card-box" id="platinum">
											<div class="content-frame">
												
												<!-- <div class="point">
													<div class="sc-master-icon icon"></div>
													18.908
												</div>	 -->
												<div class="name">Alex Abelino</div>
												<div class="date">
													<i class="far fa-calendar-alt"></i> Valid until 12/23
												</div>
											</div>
											<h6>Swipe and Scan</h6>
										</div>
									</div>
									<div class="back">
							      		<div class="card-box" id="platinum-back">
							      			<div class="qr-code">
							      				<img src="asset/images/qr/1.png">
							      			</div>
							      		</div>	
									</div>
									<br class="clear">
								</div>
							</div>
							<br class="clear">
						</div>	
					</div>
				</div>
				<div class="row payment-offer">
					<div class="col-6 col-payment popup-tg">
						<div class="card">
							<div class="background"></div>
							<div class="content">
								<b>Pay</b> <br> Now
							</div>	
						</div>	
					</div>
					<div class="col-6 col-offer" onclick="location.href='promo-index.php'">
						<div class="card">
							<div class="background"></div>
							<div class="content">
								<b>Amazing</b> <br> Offers
							</div>	
						</div>	
					</div>
				</div>
			</div>
			<div class="p-18" style="display: none">
				<div class="row row-sl">
					<div class="col-12" onclick="location.href='shopping-list.php'">
						<div class="card col-sl">
							<div class="background"></div>
							<div class="content">
								<b>Shopping</b> List
							</div>	
						</div>
					</div>
				</div>	
			</div>				
			<div class="p-18" style="display: none">
				<div class="row row-trade">
					<div class="col-12" onclick="location.href='stockbit-trade.php'">
						<div class="card col-trade">
							<div class="background"></div>
							<div class="content">
								<b>My Rewards</b> Room
							</div>
						</div>
					</div>
				</div>	
			</div>
			<div class="p-10" style="display: none">
				<div class="row index-menu">
					<div class="col-6">
						<div class="card">
							<div class="p-10">
								<div class="center" onclick="location.href='history-index.php';">
									<i class="fas fa-history"></i><br>
									Shopping <br> History
								</div>
							</div>
						</div>		
					</div>
					<div class="col-6">
						<div class="card">
							<div class="p-10">
								<div class="center" onclick="location.href='merchant-index.php';">
									<i class="fas fa-store"></i><br>
									Our <br> Merchant
								</div>
							</div>
						</div>		
					</div>
				</div>
			</div>			
			
			<div class="p-18" style="display: block;">
				<div class="row first-row loyalty-card">
					<div class="col-12">
						<div class="card-collection">
							<div class='cards'>
						      <div class='cards_inner'>
						        <div class='wrap'>
						          <div class='cards_inner__card cinemaxx'>
						            <h2></h2>
						            <span class="text"></span>
						            <div class="line-fig">
						            	<span></span>
						            	<span></span>
						            	<span></span>
						            </div>
						          </div>
						          <div class='cards_inner__card maxxcoffee'>
						            <h2></h2>
						            <span class="text"></span>
						            <div class="line-fig">
						            	<span></span>
						            	<span></span>
						            	<span></span>
						            </div>
						          </div>
						          <div class='cards_inner__card matahari'>
						            <h2></h2>
						            <span class="text"></span>
						            <div class="line-fig">
						            	<span></span>
						            	<span></span>
						            	<span></span>
						            </div>
						          </div>
						          <div class='cards_inner__card hypermart'>
						            <h2></h2>
						            <span class="text"></span>
						            <div class="line-fig">
						            	<span></span>
						            	<span></span>
						            	<span></span>
						            </div>
						          </div>
						        </div>
						      </div>
						    </div>
						    <div class='points'>
						      <div class='points_point first active'></div>
						      <div class='points_point'></div>
						      <div class='points_point'></div>
						      <div class='points_point'></div>
						    </div>
						    <h2>Swipe left throught the cards</h2>
						</div>	
					</div>
				</div>
			</div>
			<div class="p-18">
				<div class="row">
					<div class="col-12" style="display: none;">
						<div class="welcome-title">
							<h5 class="display-5">Selamat Datang</h5>
							<h3>M.De Chalidad</h3>
						</div>	
					</div>
				</div>
				<div class="row index-menu" style="display: none">
					<div class="col-3">
						<div class="p-10 center">
							<i class="fas fa-list-ul"></i><br>
							Smart List
						</div>
					</div>
					<div class="col-3">
						<div class="p-10 center">
							<i class="fas fa-history"></i><br>
							History
						</div>
					</div>
					<div class="col-3">
						<div class="p-10 center">
							<i class="fas fa-store"></i><br>
							Merchant
						</div>
					</div>
					<div class="col-3">
						<div class="p-10 center">
							<i class="fas fa-qrcode"></i><br>
							QR Code
						</div>
					</div>
				</div>			
				<div class="row index-fintech-info">
					<div class="col-12">	
						<div class="master-box card">
							<div class="top">
								<div class="p-10 row">
									<div class="col-5">
										<div class="icon"></div>
										<div class="text">
											<h3>18.908</h3>
											Loyalty Points
										</div>	
									</div>
									<div class="col-7">
										<div class="row">
											<div class="icon-box">
												<div class="icon"></div>
												Pay
											</div>
											<div class="icon-box">
												<div class="icon"></div>
												Reward
											</div>
											<div class="icon-box">
												<div class="icon"></div>
												Wallet
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="bottom">
								<div class="p-18 row">
									<div class="col-6">
										OVO Balance
									</div>
									<div class="col-6 right">
										<div class="rupiah">Rp</div>125.900
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	<!-- p18 end -->
			<!-- p18 end -->
			<div class="divider-title" style="display: block">
				<i class="fas fa-ellipsis-h right"></i>
				<div class="title">
					<h5 class="display-5"><b>Featured</b> Promo</h5>
				</div>
			</div>
			<div class="slider-frame" style="display: block">
				<div class="overflow row">
					<div class="promo-col col-9">
						<div class="thumbnail">
							<div class="images"></div>
							<div class="content">
								<div class="left">
									<span class="date">
										<i class="far fa-calendar-alt"></i> Only TODAY !
									</span>	
									<h5 class="">Indomie goreng 65gr pck</h5>
									
								</div>	
								<div class="right">
									<div class="circle-frame type-1">
										<i class="fas fa-percentage"></i>
									</div>
								</div>
							</div>
						</div>	
					</div>
					<div class="promo-col col-9">
						<div class="thumbnail">
							<div class="images"></div>
							<div class="content">
								<div class="left">
									<span class="date">
										<i class="far fa-calendar-alt"></i> Valid until Mar 4, 2019
									</span>	
									<h5 class="">LAY'S Snack Potato Chips Rachlete</h5>
									
								</div>	
								<div class="right">
									<div class="circle-frame type-2">
										<i class="fas fa-percentage"></i>
									</div>
								</div>
							</div>
						</div>	
					</div>
					<div class="promo-col col-9">
						<div class="thumbnail">
							<div class="images"></div>
							<div class="content">
								<div class="left">
									<span class="date">
										<i class="far fa-calendar-alt"></i> Valid until Feb 18, 2019
									</span>	
									<h5 class="">Unilever Lebaran MEGA SALE !</h5>
									
								</div>	
								<!-- <div class="right">
									<div class="circle-frame type-1">
										<i class="fas fa-percentage"></i>
									</div>
								</div> -->
							</div>
						</div>	
					</div>
					<div class="promo-col col-9">
						<div class="thumbnail">
							<div class="images"></div>
							<div class="content">
								<div class="left">
									<span class="date">
										<i class="far fa-calendar-alt"></i> Valid until Apr 18, 2019
									</span>	
									<h5 class="">Listerine Zero Buy 1 GET 1</h5>
									
								</div>	
								<div class="right">
									<div class="circle-frame type-3">
										<i class="fas fa-barcode"></i>
									</div>
								</div>
							</div>
						</div>	
					</div>
					<div class="promo-col col-9">
						<div class="thumbnail">
							<div class="images"></div>
							<div class="content">
								<div class="left">
									<span class="date">
										<i class="far fa-calendar-alt"></i> Valid until Aug 18, 2019
									</span>	
									<h5 class="">Nike Factory Super Merdeka PROMO!</h5>
									
								</div>	
								<div class="right">
									<div class="circle-frame type-2">
										<i class="fas fa-percentage"></i>
									</div>
								</div>
							</div>
						</div>	
					</div>
					<div class="promo-col col-9">
						<div class="thumbnail">
							<div class="images"></div>
							<div class="content">
								<div class="left">
									<span class="date">
										<i class="far fa-calendar-alt"></i> Valid until Aug 18, 2019
									</span>	
									<h5 class="">Pringless, Discount up to 40%!</h5>
									
								</div>	
								<div class="right">
									<div class="circle-frame type-1">
										<i class="fas fa-percentage"></i>
									</div>
								</div>
							</div>
						</div>	
					</div>
					<div class="promo-col col-5">
						<div class="card view-more">
							<i class="fas fa-plus"></i><br>
							view more
						</div>	
					</div>
				</div>	
			</div> <!-- p18 end -->
			<div class="p-18" style="display: block">
				<img src="asset/images/advertise-gojek.png" width="100%">
			</div>	
			<div class="divider-title" style="display: block">
				<i class="fas fa-ellipsis-h right"></i>
				<div class="title">
					<h5 class="display-5"><b>Discover</b> Promo</h5>
				</div>
			</div>
			<div class="slider-frame" style="display: block">
				<div class="overflow-full-pict row">
					<div class="full-pict-col col-6">
						<div class="full-pict">
							<div class="images"></div>
							<div class="content">
								<div class="title">
									<h3>Jakarta</h3>
									<span class="icon">
										<i class="fas fa-hand-holding-usd"></i> 2159 Promo
									</span>
								</div>
							</div>
						</div>	
					</div>
					<div class="full-pict-col col-6">
						<div class="full-pict">
							<div class="images"></div>
							<div class="content">
								<div class="title">
									<h3>Bogor</h3>
									<span class="icon">
										<i class="fas fa-hand-holding-usd"></i> 659 Promo
									</span>
								</div>
							</div>
						</div>	
					</div>
					<div class="full-pict-col col-6">
						<div class="full-pict">
							<div class="images"></div>
							<div class="content">
								<div class="title">
									<h3>Jogjakarta</h3>
									<span class="icon">
										<i class="fas fa-hand-holding-usd"></i> 418 Promo
									</span>
								</div>
							</div>
						</div>	
					</div>
					<div class="full-pict-col col-6">
						<div class="full-pict">
							<div class="images"></div>
							<div class="content">
								<div class="title">
									<h3>Bandung</h3>
									<span class="icon">
										<i class="fas fa-hand-holding-usd"></i> 1019 Promo
									</span>
								</div>
							</div>
						</div>	
					</div>
					<div class="full-pict-col col-6">
						<div class="full-pict">
							<div class="images"></div>
							<div class="content">
								<div class="title">
									<h3>Surabaya</h3>
									<span class="icon">
										<i class="fas fa-hand-holding-usd"></i> 763 Promo
									</span>
								</div>
							</div>
						</div>	
					</div>
					<div class="promo-col col-5">
						<div class="card view-more">
							<i class="fas fa-plus"></i><br>
							view more
						</div>	
					</div>
				</div>	
			</div> <!-- p18 end -->

			<div class="popup">
				<div class="p-18">
					<div class="button-close">
						<i class="fas fa-times"></i>
					</div>
					<div class="popup-header">
						<h3><b>Payment</b> Method</h3>
					</div>
					<div class="popup-body">
						<div class="payment-method row">
							<div class="col-6">
								<div class="card ovo" onclick="window.open('https://www.ovo.id/', '_blank');">
									<div class="p-10 center">
										<div class="icon ovo"><img src="asset/images/icon-ovo.png" alt=""></div>
										<span class="text"><b>OVO</b></span>
									</div>
								</div>
							</div>
							<div class="col-6">
								<div class="card gojek" onclick="window.open('https://www.go-jek.com/go-pay/', '_blank');">
									<div class="p-10 center">
										<div class="icon gojek"><img src="asset/images/icon-gopay.png" alt=""></div>
										<span class="text"><b>GOPAY</b></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="popup-footer"></div>
				</div>
			</div>

			<div class="footer-frame"></div>
		</div><!-- master body -->

		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
		<script>
			t = 53;
			p = 0;
			pm = $('.cards_inner__card').length;

			$('.cards_inner__card').mousedown(function(){
			  var ct = $(this).css('transform');
			  var cts = ct.split(',')
			  ctse = (cts[cts.length - 2] + 'px')
			})

			function on(){
			  $('.cards_inner__card').draggable({
			    start: function( event, ui ) {
			      startPosition = ui.position.left;
			    },
			    drag:function(e, ui){
			      if(ui.position.left > startPosition){
			        ui.position.left = startPosition;
			      }
			      if(ui.position.left < -250){
			        ui.position.left = -250;
			      }
			      x = ui.position.left;
			      $(this).css('transform',' rotate(' + x/36 + 'deg)')
			    },
			    revert:function(valid) {
			      if(x > 1 || x < - 1) {
			        el = $(this)
			        setTimeout(function(){
			          el_class = el.attr('class').split(' ');
			          el_class_end = el_class[1]
			          el.addClass('invalid')
			          if(p < 3){
			            $('.points').find('.active').removeClass('active').next().addClass('active') 
			            p++
			          } else {
			            $('.points').find('.active').removeClass('active')
			            $('.points').find('.first').addClass('active') 
			            p=0
			          }
			        },10)
			        setTimeout(function(){
			          $('.cards_inner__card').each(function(){
			            $(this).addClass('animate');
			            var ct = $(this).css('transform');
			            var cts = ct.split(',')
			            ctse = (parseInt(cts[cts.length - 2]) + 60 + 'px')
			            $(this).css('transform','translateZ(' + ctse + ')');
			          });
			          $('.cards_inner .wrap').prepend('<div class="cards_inner__card ' + el_class_end + ' card_in"><h2></h2><span class="text"></span><div class="line-fig"><span></span><span></span><span></span></div></div>')
			          el.remove();
			          $('.cards_inner__card').removeClass('animate');
			          on();
			        },160);
			        setTimeout(function(){
			          $('.card_in').removeClass('card_in')
			        },500);
			      } else {
			        $(this).css('transform','rotate(0deg)')
			        return !valid;
			      }
			    },
			    axis:'x',
			    containment:'.cards_inner'
			  });
			  $('.cards_inner__card:nth-of-type(1)').draggable( 'disable' )
			  $('.cards_inner__card:nth-of-type(2)').draggable( 'disable' )
			  $('.cards_inner__card:nth-of-type(3)').draggable( 'disable' )
			  $('.cards_inner__card:nth-of-type(4)').draggable( 'enable' )
			}
			on();

		</script>
	</body>
</html>

