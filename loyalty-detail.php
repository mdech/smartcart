<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>SmartCart Mockup</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="shortcut icon" type="image" href="asset/images/gaspace-icon.png">
		<link rel="stylesheet" type="text/css" href="asset/css/loyalty-detail.css" media="all"/>
		<link rel="stylesheet" type="text/css" href="asset/css/animation.css" media="all"/>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
		<script src="asset/js/jquery-2.1.1.js"></script>
		<script src="asset/js/jquery-ui.js"></script>
		<script src="asset/js/smartcart-master.js"></script>
		
	</head>
	<body>
		<div class="master-body">
			<div class="navbar-top">
				<div class="center-logo"></div>
				<div class="right-frame">
					<i class="fas fa-user"></i>
				</div>
			</div>
			<div class="navbar-bottom row">
				<div class="col-3 list" onclick="location.href='index.php';">
					<div class="icon"></div>
					<div class="label">
						Home
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list" onclick="location.href='promo-index.php';">
					<div class="icon"></div>
					<div class="label">
						Promo
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list" onclick="location.href='nearby-index.php';">
					<div class="icon"></div>
					<div class="label">
						Nearby
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list selected">
					<div class="icon"></div>
					<div class="label">
						Target
					</div>	
					<span class="clear"></span>
				</div>
			</div>

			<!-- S T A R T   C U S T O M I Z A T I O N   F R O M   H E R E -->

			<div class="header loyalty"> <!-- Header -->
				<div class="p-18">
					<!-- <div class="bottom-image"></div> -->
				</div>
			</div>
			<div class="p-18">
				<div class="row index-fintech-info">
					<div class="col-12">	
						<div class="master-box card">
							<div class="top">
								<div class="p-10 row">
									<div class="col-5">
										<div class="icon"></div>
										<div class="text">
											<h3>18.908</h3>
											Loyalty Points
										</div>	
									</div>
									<div class="col-7">
										<div class="row">
											<div class="icon-box">
												<div class="icon"></div>
												Pay
											</div>
											<div class="icon-box">
												<div class="icon"></div>
												Reward
											</div>
											<div class="icon-box">
												<div class="icon"></div>
												Wallet
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="bottom">
								<div class="p-18 row">
									<div class="col-6">
										OVO Balance
									</div>
									<div class="col-6 right">
										<div class="rupiah">Rp</div>125.900
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	
				<div class="loyalty-detail-2">
					<div class="row pull-row">
						<div class="col-4 coupon-tab active">	
							<div class="card">
								Amazing <br> Offers	
							</div>
						</div>
						<div class="col-4 exclusive-tab">	
							<div class="card">
								Shopping <br> List
							</div>
						</div>
						<div class="col-4 third-tab">	
							<div class="card">
								My <br> Trades
							</div>
						</div>	
					</div>
					
					<div class="tab-frame">
						<div class="slide">

							<div class="tab coupon active">
								<div class="divider-title">
									<i class="fas fa-ellipsis-h right"></i>
									<div class="title">
										<h5 class="display-5"><b>Amazing</b> Offers</h5>
									</div>
								</div>
								<div class="slider-frame">
									<div class="overflow row">
										<div class="promo-col col-9">
											<div class="thumbnail">
												<div class="images"></div>
												<div class="content">
													<div class="left">
														<span class="date">
															<i class="far fa-calendar-alt"></i> Only TODAY !
														</span>	
														<h5 class="">Indomie goreng 65gr pck</h5>
														
													</div>	
													<div class="right">
														<div class="circle-frame type-1">
															<i class="fas fa-percentage"></i>
														</div>
													</div>
												</div>
											</div>	
										</div>
										<div class="promo-col col-9">
											<div class="thumbnail">
												<div class="images"></div>
												<div class="content">
													<div class="left">
														<span class="date">
															<i class="far fa-calendar-alt"></i> Valid until Mar 4, 2019
														</span>	
														<h5 class="">LAY'S Snack Potato Chips Rachlete</h5>
														
													</div>	
													<div class="right">
														<div class="circle-frame type-2">
															<i class="fas fa-percentage"></i>
														</div>
													</div>
												</div>
											</div>	
										</div>
										<div class="promo-col col-9">
											<div class="thumbnail">
												<div class="images"></div>
												<div class="content">
													<div class="left">
														<span class="date">
															<i class="far fa-calendar-alt"></i> Valid until Feb 18, 2019
														</span>	
														<h5 class="">Unilever Lebaran MEGA SALE !</h5>
														
													</div>	
													<!-- <div class="right">
														<div class="circle-frame type-1">
															<i class="fas fa-percentage"></i>
														</div>
													</div> -->
												</div>
											</div>	
										</div>
										<div class="promo-col col-9">
											<div class="thumbnail">
												<div class="images"></div>
												<div class="content">
													<div class="left">
														<span class="date">
															<i class="far fa-calendar-alt"></i> Valid until Apr 18, 2019
														</span>	
														<h5 class="">Listerine Zero Buy 1 GET 1</h5>
														
													</div>	
													<div class="right">
														<div class="circle-frame type-3">
															<i class="fas fa-barcode"></i>
														</div>
													</div>
												</div>
											</div>	
										</div>
										<div class="promo-col col-9">
											<div class="thumbnail">
												<div class="images"></div>
												<div class="content">
													<div class="left">
														<span class="date">
															<i class="far fa-calendar-alt"></i> Valid until Aug 18, 2019
														</span>	
														<h5 class="">Nike Factory Super Merdeka PROMO!</h5>
														
													</div>	
													<div class="right">
														<div class="circle-frame type-2">
															<i class="fas fa-percentage"></i>
														</div>
													</div>
												</div>
											</div>	
										</div>
										<div class="promo-col col-9">
											<div class="thumbnail">
												<div class="images"></div>
												<div class="content">
													<div class="left">
														<span class="date">
															<i class="far fa-calendar-alt"></i> Valid until Aug 18, 2019
														</span>	
														<h5 class="">Pringless, Discount up to 40%!</h5>
														
													</div>	
													<div class="right">
														<div class="circle-frame type-1">
															<i class="fas fa-percentage"></i>
														</div>
													</div>
												</div>
											</div>	
										</div>
										<div class="promo-col col-5">
											<div class="card view-more">
												<i class="fas fa-plus"></i><br>
												view more
											</div>	
										</div>
									</div>	
								</div> 
								<div class="divider-title">
									<i class="fas fa-ellipsis-h right"></i>
									<div class="title">
										<h5 class="display-5"><b>Exclusive</b> Coupon</h5>
									</div>
								</div>
								<div class="p-18">
									<div class="row">
										<div class="col-12">
											<div class="coupon-frame card type-2">
												<div class="padding">
													<div class="tenant-info">
														<div class="logo">
															<img src="asset/images/tenant-logo-1.png">
														</div>
														<div class="title">Bread life</div>
														<Br class="clear">
													</div>
													<div class="disc-info">
														<h4>50% OFF <br>Paket 6 Roti Bite Series</h4>
														<div>Berlaku hanya untuk pembelian dengan app untuk pertama kali</div>
													</div>
													<div class="footer">
														<div class="border-top"></div>
														<div class="row">
															<div class="col-4">
																<div class="icon">
																	<i class="far fa-calendar-alt"></i>
																</div>
																<div class="text">
																	Exp Date
																	<div class="date">21 Feb 2018</div>	
																</div>	
															</div>
															<div class="col-8">
																<div class="icon">
																	<i class="fas fa-award"></i>
																</div>	
																<div class="text">
																	Used
																	<div>102 times <br>(1230 remaining)</div>
																</div>	
															</div>	
														</div>	
													</div>
												</div>
											</div>
											<div class="coupon-frame card type-1">
												<div class="padding">
													<div class="tenant-info">
														<div class="logo">
															<img src="asset/images/tenant-logo-2.png">
														</div>
														<div class="title">D'crepes</div>
														<Br class="clear">
													</div>
													<div class="disc-info">
														<h4>Cashback 30%</h4>
														<div>Khusus pembayaran di tempat menggunakan GO-PAY</div>
													</div>
													<div class="footer">
														<div class="border-top"></div>
														<div class="row">
															<div class="col-4">
																<div class="icon">
																	<i class="far fa-calendar-alt"></i>
																</div>
																<div class="text">
																	Exp Date
																	<div class="date">7 Mar 2018</div>	
																</div>	
															</div>
															<div class="col-8">
																<div class="icon">
																	<i class="fas fa-award"></i>
																</div>	
																<div class="text">
																	Used
																	<div>18 times <br>(90 remaining)</div>
																</div>	
															</div>	
														</div>	
													</div>
												</div>
											</div>
											<div class="coupon-frame card type-3">
												<div class="padding">
													<div class="tenant-info">
														<div class="logo">
															<img src="asset/images/tenant-logo-3.png">
														</div>
														<div class="title">Dum Dum Thai Tea</div>
														<Br class="clear">
													</div>
													<div class="disc-info">
														<h4>Buy 1 Get 1 !</h4>
														<div>Khusus pembayaran di tempat menggunakan GO-PAY</div>
													</div>
													<div class="footer">
														<div class="border-top"></div>
														<div class="row">
															<div class="col-4">
																<div class="icon">
																	<i class="far fa-calendar-alt"></i>
																</div>
																<div class="text">
																	Exp Date
																	<div class="date">1 Des 2019</div>	
																</div>	
															</div>
															<div class="col-8">
																<div class="icon">
																	<i class="fas fa-award"></i>
																</div>	
																<div class="text">
																	Used
																	<div>0 times <br>(2000 remaining)</div>
																</div>	
															</div>	
														</div>	
													</div>
												</div>
											</div>
											<div class="coupon-frame card type-1">
												<div class="padding">
													<div class="tenant-info">
														<div class="logo">
															<img src="asset/images/tenant-logo-1.png">
														</div>
														<div class="title">Shihlin Taiwan snacks</div>
														<Br class="clear">
													</div>
													<div class="disc-info">
														<h4>Discount up to 70%</h4>
														<div>Hanya untuk menu baru terpilih</div>
													</div>
													<div class="footer">
														<div class="border-top"></div>
														<div class="row">
															<div class="col-4">
																<div class="icon">
																	<i class="far fa-calendar-alt"></i>
																</div>
																<div class="text">
																	Exp Date
																	<div class="date">12 Jun 2019</div>	
																</div>	
															</div>
															<div class="col-8">
																<div class="icon">
																	<i class="fas fa-award"></i>
																</div>	
																<div class="text">
																	Used
																	<div>12 times <br>(15 remaining)</div>
																</div>	
															</div>	
														</div>	
													</div>
												</div>
											</div>
											<div class="coupon-frame card type-4">
												<div class="padding">
													<div class="tenant-info">
														<div class="logo">
															<img src="asset/images/tenant-logo-5.png">
														</div>
														<div class="title">Chatime</div>
														<Br class="clear">
													</div>
													<div class="disc-info">
														<h4>Discount up to 70%</h4>
														<div>Hanya untuk menu baru terpilih</div>
													</div>
													<div class="footer">
														<div class="border-top"></div>
														<div class="row">
															<div class="col-4">
																<div class="icon">
																	<i class="far fa-calendar-alt"></i>
																</div>
																<div class="text">
																	Exp Date
																	<div class="date">12 Jun 2019</div>	
																</div>	
															</div>
															<div class="col-8">
																<div class="icon">
																	<i class="fas fa-award"></i>
																</div>	
																<div class="text">
																	Used
																	<div>12 times <br>(15 remaining)</div>
																</div>	
															</div>	
														</div>	
													</div>
												</div>
											</div>
										</div>	
									</div>
								</div>	
							</div>	

							<div class="tab exclusive">

								<div class="divider-title">
									<i class="fas fa-ellipsis-h right"></i>
									<div class="title">
										<h5 class="display-5"><b>Shopping</b> List</h5>
									</div>
								</div>
								<div class="slider-frame">
									<div class="overflow-sl-sm row">
										<div class="sl-sm-col col-9">
											<div class="card">
												<div class="p-10">
													<div class="image">
														<img src="asset/images/sl-image-popmie.jpg">
													</div>
													<div class="content">
														<div class="name">POP MIE Baso Special Jumbo 75gr</div>
														<div class="price"><span>Rp</span> 6,000</div>
														<div class="add-item-box">
															<div class="icon">
																<i class="fas fa-minus"></i>
															</div>
															<div class="center">2</div>
															<div class="icon">
																<i class="fas fa-plus"></i>
															</div>
														</div>
														<br class="clear">
													</div>
													<br class="clear">
												</div>
											</div>
											<div class="card">
												<div class="p-10">
													<div class="image">
														<img src="asset/images/sl-image-bayfresh.jpg">
													</div>
													<div class="content">
														<div class="name">BAYFRESH Pop Scent Lovely 10gr</div>
														<div class="price"><span>Rp</span> 9,000</div>
														<div class="add-item-box">
															<div class="icon">
																<i class="fas fa-minus"></i>
															</div>
															<div class="center">2</div>
															<div class="icon">
																<i class="fas fa-plus"></i>
															</div>
														</div>
														<br class="clear">
													</div>
													<br class="clear">
												</div>
											</div>
											<div class="card">
												<div class="p-10">
													<div class="image">
														<img src="asset/images/sl-image-gowell.jpg">
													</div>
													<div class="content">
														<div class="name">GOWELL Rasa Taro 5x29gr</div>
														<div class="price"><span>Rp</span> 6,500</div>
														<div class="add-item-box">
															<div class="icon">
																<i class="fas fa-minus"></i>
															</div>
															<div class="center">3</div>
															<div class="icon">
																<i class="fas fa-plus"></i>
															</div>
														</div>
														<br class="clear">
													</div>
													<br class="clear">
												</div>
											</div>
										</div>
										<div class="sl-sm-col col-9">
											<div class="card">
												<div class="p-10">
													<div class="image">
														<img src="asset/images/sl-image-indomie.jpg">
													</div>
													<div class="content">
														<div class="name">INDOMIE New Kari 72gr</div>
														<div class="price"><span>Rp</span> 3,000</div>
														<div class="add-item-box">
															<div class="icon">
																<i class="fas fa-minus"></i>
															</div>
															<div class="center">12</div>
															<div class="icon">
																<i class="fas fa-plus"></i>
															</div>
														</div>
														<br class="clear">
													</div>
													<br class="clear">
												</div>
											</div>
											<div class="card">
												<div class="p-10">
													<div class="image">
														<img src="asset/images/sl-image-ovomaltine.jpg">
													</div>
													<div class="content">
														<div class="name">OVOMALTINE Crunchy Cream 380gr (BPOM)</div>
														<div class="price"><span>Rp</span> 72,000</div>
														<div class="add-item-box">
															<div class="icon">
																<i class="fas fa-minus"></i>
															</div>
															<div class="center">1</div>
															<div class="icon">
																<i class="fas fa-plus"></i>
															</div>
														</div>
														<br class="clear">
													</div>
													<br class="clear">
												</div>
											</div>
											<div class="card">
												<div class="p-10">
													<div class="image">
														<img src="asset/images/sl-image-bango.png">
													</div>
													<div class="content">
														<div class="name">BANGO Kecap Manis Pouch 575ml</div>
														<div class="price"><span>Rp</span> 23,500</div>
														<div class="add-item-box">
															<div class="icon">
																<i class="fas fa-minus"></i>
															</div>
															<div class="center">1</div>
															<div class="icon">
																<i class="fas fa-plus"></i>
															</div>
														</div>
														<br class="clear">
													</div>
													<br class="clear">
												</div>
											</div>
										</div>
										<div class="sl-sm-col col-9">
											<div class="card">
												<div class="p-10">
													<div class="image">
														<img src="asset/images/sl-image-potabee.jpg">
													</div>
													<div class="content">
														<div class="name">POTABEE Regular Beef BBQ 68g</div>
														<div class="price"><span>Rp</span> 8,000</div>
														<div class="add-item-box">
															<div class="icon">
																<i class="fas fa-minus"></i>
															</div>
															<div class="center">5</div>
															<div class="icon">
																<i class="fas fa-plus"></i>
															</div>
														</div>
														<br class="clear">
													</div>
													<br class="clear">
												</div>
											</div>
											<div class="card">
												<div class="p-10">
													<div class="image">
														<img src="asset/images/sl-image-rosebrand.jpg">
													</div>
													<div class="content">
														<div class="name">ROSE BRAND Gula Kristal Premium 1 kg</div>
														<div class="price"><span>Rp</span> 12,000</div>
														<div class="add-item-box">
															<div class="icon">
																<i class="fas fa-minus"></i>
															</div>
															<div class="center">4</div>
															<div class="icon">
																<i class="fas fa-plus"></i>
															</div>
														</div>
														<br class="clear">
													</div>
													<br class="clear">
												</div>
											</div>
											<div class="card">
												<div class="p-10">
													<div class="image">
														<img src="asset/images/sl-image-sariwangi.jpg">
													</div>
													<div class="content">
														<div class="name">SARIWANGI Teh Asli 50 The Celup</div>
														<div class="price"><span>Rp</span> 10,500</div>
														<div class="add-item-box">
															<div class="icon">
																<i class="fas fa-minus"></i>
															</div>
															<div class="center">3</div>
															<div class="icon">
																<i class="fas fa-plus"></i>
															</div>
														</div>
														<br class="clear">
													</div>
													<br class="clear">
												</div>
											</div>
										</div>
										<div class="sl-sm-col col-9">
											<div class="card">
												<div class="p-10">
													<div class="image">
														<img src="asset/images/sl-image-kapalapi.jpg">
													</div>
													<div class="content">
														<div class="name">Kapal Api Gula Rasa Mantap Bag 25gr x 30 pcs</div>
														<div class="price"><span>Rp</span> 32,000</div>
														<div class="add-item-box">
															<div class="icon">
																<i class="fas fa-minus"></i>
															</div>
															<div class="center">1</div>
															<div class="icon">
																<i class="fas fa-plus"></i>
															</div>
														</div>
														<br class="clear">
													</div>
													<br class="clear">
												</div>
											</div>
											<div class="card">
												<div class="p-10">
													<div class="image">
														<img src="asset/images/sl-image-sunlight.jpg">
													</div>
													<div class="content">
														<div class="name">SUNLIGHT Lime Refill 780ml</div>
														<div class="price"><span>Rp</span> 15,000</div>
														<div class="add-item-box">
															<div class="icon">
																<i class="fas fa-minus"></i>
															</div>
															<div class="center">3</div>
															<div class="icon">
																<i class="fas fa-plus"></i>
															</div>
														</div>
														<br class="clear">
													</div>
													<br class="clear">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="tab third">
								
								<div class="divider-title">
									<i class="fas fa-ellipsis-h right"></i>
									<div class="title">
										<h5 class="display-5"><b>My</b> Trades</h5>
									</div>
								</div>
								<div class="trades-wrapper">
									<div class="p-18">
										<img src="asset/images/my-trades.jpg" alt="" class="my-trade-ss">
									</div>
								</div>
							</div>
							<br class="clear">
						</div>	
					</div>			
				</div>
			</div>	
			<div class="footer-frame"></div>
		</div><!-- master body -->
	</body>
</html>

