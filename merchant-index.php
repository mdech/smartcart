<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>SmartCart Mockup</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="shortcut icon" type="image" href="asset/images/gaspace-icon.png">
		<link rel="stylesheet" type="text/css" href="asset/css/merchant-index.css" media="all"/>
		<link rel="stylesheet" type="text/css" href="asset/css/animation.css" media="all"/>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
		<script src="asset/js/jquery-2.1.1.js"></script>
		<script src="asset/js/jquery-ui.js"></script>
		<script src="asset/js/smartcart-master.js"></script>
		
	</head>
	<body>
		<div class="master-body">
			<div class="navbar-top">
				<div class="back-button" onclick="location.href='index.php'">
					<i class="fas fa-chevron-left"></i>
				</div>
			</div>
			<div class="navbar-bottom row">
				<div class="col-3 list" onclick="location.href='index.php';">
					<div class="icon"></div>
					<div class="label">
						Home
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list" onclick="location.href='promo-index.php';">
					<div class="icon"></div>
					<div class="label">
						Promo
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list" onclick="location.href='nearby-index.php';">
					<div class="icon"></div>
					<div class="label">
						Nearby
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list" onclick="location.href='credit-score.php'">
					<div class="icon"></div>
					<div class="label">
						Target
					</div>	
					<span class="clear"></span>
				</div>
			</div>

			<!-- S T A R T   C U S T O M I Z A T I O N   F R O M   H E R E -->

			<div class="header sub-menu"> <!-- Header -->
				<div class="p-18">
					<!-- <div class="bottom-image"></div> -->
				</div>
			</div>
			<div class="p-18">
				<div class="row row-merchant-1">
					<div class="col-12">
						<h4 class="display-4"><b>Our</b> Merchant</h4>
					</div>
				</div>	
				
			</div>
			<div class="search-bar-sticky">
				<div class="col-12">
					<div class="card">
						<div class="p-18">
							<i class="fas fa-search left"></i>
							<input class="input-search" type="text" placeholder="Search Merchant ..." required>
							<i class="fas fa-filter right"></i>
							<br class="clear">
						</div>
					</div>
				</div>
			</div>
			<div class="divider-title">
				<i class="fas fa-ellipsis-h right"></i>
				<div class="title">
					<h5 class="display-5"><b>Weekly</b> Top Merchant</h5>
				</div>
			</div>
			<div class="slider-frame">
				<div class="overflow-merchant row">
					<div class="merchant-col col-4">
						<div class="card">
							<div class="p-15">
								<img src="asset/images/merchant-logo-1.jpg" class="images">
								<div class="content">
									<div class="title">Bread life</div>
									<div class="subtitle">
										<i class="fas fa-store"></i> 42 
										<i class="fas fa-money-bill-wave"></i> 10
									</div>
								</div>
							</div>	
						</div>	
					</div>
					<div class="merchant-col col-4">
						<div class="card">
							<div class="p-15">
								<img src="asset/images/merchant-logo-2.jpg" class="images">
								<div class="content">
									<div class="title">D'Crepes</div>
									<div class="subtitle">
										<i class="fas fa-store"></i> 13 
										<i class="fas fa-money-bill-wave"></i> 7
									</div>
								</div>
							</div>	
						</div>	
					</div>
					<div class="merchant-col col-4">
						<div class="card">
							<div class="p-15">
								<img src="asset/images/merchant-logo-3.jpg" class="images">
								<div class="content">
									<div class="title">Dum Dum</div>
									<div class="subtitle">
										<i class="fas fa-store"></i> 18 
										<i class="fas fa-money-bill-wave"></i> 2
									</div>
								</div>
							</div>	
						</div>	
					</div>
					<div class="merchant-col col-4">
						<div class="card">
							<div class="p-15">
								<img src="asset/images/merchant-logo-4.jpg" class="images">
								<div class="content">
									<div class="title">Shihlin</div>
									<div class="subtitle">
										<i class="fas fa-store"></i> 7 
										<i class="fas fa-money-bill-wave"></i> 11
									</div>
								</div>
							</div>	
						</div>	
					</div>
					<div class="merchant-col col-4">
						<div class="card">
							<div class="p-15">
								<img src="asset/images/merchant-logo-5.jpg" class="images">
								<div class="content">
									<div class="title">Chatime</div>
									<div class="subtitle">
										<i class="fas fa-store"></i> 15
										<i class="fas fa-money-bill-wave"></i> 2
									</div>
								</div>
							</div>	
						</div>	
					</div>
					<div class="merchant-col col-4">
						<div class="card">
							<div class="p-15">
								<img src="asset/images/merchant-logo-6.jpg" class="images">
								<div class="content">
									<div class="title">Ace Hardware</div>
									<div class="subtitle">
										<i class="fas fa-store"></i> 22 
										<i class="fas fa-money-bill-wave"></i> 8
									</div>
								</div>
							</div>	
						</div>	
					</div>
				</div>	
			</div> 

			<div class="divider-title">
				<i class="fas fa-ellipsis-h right"></i>
				<div class="title">
					<h5 class="display-5"><b>All</b> Merchant</h5>
				</div>
			</div>
			<div class="p-18">
				<div class="row row-merchant-list">
					<div class="merchant-col col-4">
						<div class="card">
							<div class="p-15">
								<img src="asset/images/merchant-logo-7.jpg" class="images">
								<div class="content">
									<div class="title">Bread life</div>
									<div class="subtitle">
										<i class="fas fa-store"></i> 42 
										<i class="fas fa-money-bill-wave"></i> 10
									</div>
								</div>
							</div>	
						</div>	
					</div>
					<div class="merchant-col col-4">
						<div class="card">
							<div class="p-15">
								<img src="asset/images/merchant-logo-8.jpg" class="images">
								<div class="content">
									<div class="title">D'Crepes</div>
									<div class="subtitle">
										<i class="fas fa-store"></i> 13 
										<i class="fas fa-money-bill-wave"></i> 7
									</div>
								</div>
							</div>	
						</div>	
					</div>
					<div class="merchant-col col-4">
						<div class="card">
							<div class="p-15">
								<img src="asset/images/merchant-logo-9.jpg" class="images">
								<div class="content">
									<div class="title">Dum Dum</div>
									<div class="subtitle">
										<i class="fas fa-store"></i> 18 
										<i class="fas fa-money-bill-wave"></i> 2
									</div>
								</div>
							</div>	
						</div>	
					</div>
				</div>
				<div class="row row-merchant-list">
					<div class="merchant-col col-4">
						<div class="card">
							<div class="p-15">
								<img src="asset/images/merchant-logo-10.jpg" class="images">
								<div class="content">
									<div class="title">Bread life</div>
									<div class="subtitle">
										<i class="fas fa-store"></i> 42 
										<i class="fas fa-money-bill-wave"></i> 10
									</div>
								</div>
							</div>	
						</div>	
					</div>
					<div class="merchant-col col-4">
						<div class="card">
							<div class="p-15">
								<img src="asset/images/merchant-logo-11.jpg" class="images">
								<div class="content">
									<div class="title">D'Crepes</div>
									<div class="subtitle">
										<i class="fas fa-store"></i> 13 
										<i class="fas fa-money-bill-wave"></i> 7
									</div>
								</div>
							</div>	
						</div>	
					</div>
					<div class="merchant-col col-4">
						<div class="card">
							<div class="p-15">
								<img src="asset/images/merchant-logo-12.jpg" class="images">
								<div class="content">
									<div class="title">Dum Dum</div>
									<div class="subtitle">
										<i class="fas fa-store"></i> 18 
										<i class="fas fa-money-bill-wave"></i> 2
									</div>
								</div>
							</div>	
						</div>	
					</div>
				</div>
				<div class="row row-merchant-list">
					<div class="merchant-col col-4">
						<div class="card">
							<div class="p-15">
								<img src="asset/images/merchant-logo-13.jpg" class="images">
								<div class="content">
									<div class="title">Bread life</div>
									<div class="subtitle">
										<i class="fas fa-store"></i> 42 
										<i class="fas fa-money-bill-wave"></i> 10
									</div>
								</div>
							</div>	
						</div>	
					</div>
					<div class="merchant-col col-4">
						<div class="card">
							<div class="p-15">
								<img src="asset/images/merchant-logo-14.jpg" class="images">
								<div class="content">
									<div class="title">D'Crepes</div>
									<div class="subtitle">
										<i class="fas fa-store"></i> 13 
										<i class="fas fa-money-bill-wave"></i> 7
									</div>
								</div>
							</div>	
						</div>	
					</div>
					<div class="merchant-col col-4">
						<div class="card">
							<div class="p-15">
								<img src="asset/images/merchant-logo-15.jpg" class="images">
								<div class="content">
									<div class="title">D'Crepes</div>
									<div class="subtitle">
										<i class="fas fa-store"></i> 13 
										<i class="fas fa-money-bill-wave"></i> 7
									</div>
								</div>
							</div>	
						</div>	
					</div>
				</div>
				<div class="row row-merchant-list">
					<div class="merchant-col col-4">
						<div class="card">
							<div class="p-15">
								<img src="asset/images/merchant-logo-16.jpg" class="images">
								<div class="content">
									<div class="title">Bread life</div>
									<div class="subtitle">
										<i class="fas fa-store"></i> 42 
										<i class="fas fa-money-bill-wave"></i> 10
									</div>
								</div>
							</div>	
						</div>	
					</div>
					<div class="merchant-col col-4">
						<div class="card">
							<div class="p-15">
								<img src="asset/images/merchant-logo-17.jpg" class="images">
								<div class="content">
									<div class="title">D'Crepes</div>
									<div class="subtitle">
										<i class="fas fa-store"></i> 13 
										<i class="fas fa-money-bill-wave"></i> 7
									</div>
								</div>
							</div>	
						</div>	
					</div>
					<div class="merchant-col col-4">
						<div class="card">
							<div class="p-15">
								<img src="asset/images/merchant-logo-18.jpg" class="images">
								<div class="content">
									<div class="title">D'Crepes</div>
									<div class="subtitle">
										<i class="fas fa-store"></i> 13 
										<i class="fas fa-money-bill-wave"></i> 7
									</div>
								</div>
							</div>	
						</div>	
					</div>
				</div>
				<div class="row row-merchant-list">
					<div class="merchant-col col-4">
						<div class="card">
							<div class="p-15">
								<img src="asset/images/merchant-logo-19.jpg" class="images">
								<div class="content">
									<div class="title">Bread life</div>
									<div class="subtitle">
										<i class="fas fa-store"></i> 42 
										<i class="fas fa-money-bill-wave"></i> 10
									</div>
								</div>
							</div>	
						</div>	
					</div>
					<div class="merchant-col col-4">
						<div class="card">
							<div class="p-15">
								<img src="asset/images/merchant-logo-20.jpg" class="images">
								<div class="content">
									<div class="title">D'Crepes</div>
									<div class="subtitle">
										<i class="fas fa-store"></i> 13 
										<i class="fas fa-money-bill-wave"></i> 7
									</div>
								</div>
							</div>	
						</div>	
					</div>
				</div>
			</div>
			<div class="footer-frame"></div>
		</div><!-- master body -->
	</body>
</html>

