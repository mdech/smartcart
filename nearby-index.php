<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>SmartCart Mockup</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="shortcut icon" type="image" href="asset/images/gaspace-icon.png">
		<link rel="stylesheet" type="text/css" href="asset/css/nearby-index.css" media="all"/>
		<link rel="stylesheet" type="text/css" href="asset/css/animation.css" media="all"/>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
		<script src="asset/js/jquery-2.1.1.js"></script>
		<script src="asset/js/jquery-ui.js"></script>
		<script src="asset/js/smartcart-master.js"></script>
		
	</head>
	<body>
		<div class="master-body">
			<div class="navbar-top">
				<div class="center-logo"></div>
				<div class="right-frame">
					<i class="fas fa-user"></i>
				</div>
			</div>
			<div class="navbar-bottom row">
				<div class="col-3 list" onclick="location.href='index.php';">
					<div class="icon"></div>
					<div class="label">
						Home
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list" onclick="location.href='promo-index.php';">
					<div class="icon"></div>
					<div class="label">
						Promo
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list selected" onclick="location.href='nearby-index.php';">
					<div class="icon"></div>
					<div class="label">
						Nearby
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list" onclick="location.href='credit-score.php';">
					<div class="icon"></div>
					<div class="label">
						Target
					</div>	
					<span class="clear"></span>
				</div>
			</div>

			<!-- S T A R T   C U S T O M I Z A T I O N   F R O M   H E R E -->

			<div class="header nearby-index jakarta"> <!-- Header -->
				<div class="p-18">
					<!-- <div class="bottom-image"></div> -->
				</div>
			</div>

			<div class="p-18"> <!-- body -->	
				<div class="row nearby-header">
					<div class="content">
						<div class="title">Jakarta</div>
						<div class="sub-title">
							<i class="fas fa-store"></i> 189 Merchant | 
							<i class="fas fa-hand-holding-usd"></i> 302 Promos
						</div>
					</div>	
				</div>
			</div>
			<div class="promo-index-2">
				<div class="col-12">
					<div class="card">
						<div class="p-18">
							<i class="fas fa-search left"></i>
							<input class="input-search" type="text" placeholder="Im interested in ..." required>
							<i class="fas fa-filter right"></i>
							<br class="clear">
						</div>
					</div>
				</div>
			</div>
			<br class="clear">
			<div class="divider-title">
				<i class="fas fa-ellipsis-h right"></i>
				<div class="title">
					<h4 class="display-5"><b>Today's Best</b> Offers</h4>
				</div>
			</div>
			<div class="slider-frame">
				<div class="overflow row">
					<div class="promo-col col-9">
						<div class="thumbnail">
							<div class="images"></div>
							<div class="content">
								<div class="left">
									<span class="date">
										<i class="far fa-calendar-alt"></i> Only TODAY !
									</span>	
									<h4 class="">Indomie goreng 65gr pck</h4>
									
								</div>	
								<div class="right">
									<div class="circle-frame type-1">
										<i class="fas fa-percentage"></i>
									</div>
								</div>
							</div>
						</div>	
					</div>
					<div class="promo-col col-9">
						<div class="thumbnail">
							<div class="images"></div>
							<div class="content">
								<div class="left">
									<span class="date">
										<i class="far fa-calendar-alt"></i> Valid until Mar 4, 2019
									</span>	
									<h4 class="">LAY'S Snack Potato Chips Rachlete</h4>
									
								</div>	
								<div class="right">
									<div class="circle-frame type-2">
										<i class="fas fa-percentage"></i>
									</div>
								</div>
							</div>
						</div>	
					</div>
					<div class="promo-col col-9">
						<div class="thumbnail">
							<div class="images"></div>
							<div class="content">
								<div class="left">
									<span class="date">
										<i class="far fa-calendar-alt"></i> Valid until Feb 18, 2019
									</span>	
									<h4 class="">Unilever Lebaran MEGA SALE !</h4>
									
								</div>	
								<!-- <div class="right">
									<div class="circle-frame type-1">
										<i class="fas fa-percentage"></i>
									</div>
								</div> -->
							</div>
						</div>	
					</div>
					<div class="promo-col col-9">
						<div class="thumbnail">
							<div class="images"></div>
							<div class="content">
								<div class="left">
									<span class="date">
										<i class="far fa-calendar-alt"></i> Valid until Apr 18, 2019
									</span>	
									<h4 class="">Listerine Zero Buy 1 GET 1</h4>
									
								</div>	
								<div class="right">
									<div class="circle-frame type-3">
										<i class="fas fa-barcode"></i>
									</div>
								</div>
							</div>
						</div>	
					</div>
					<div class="promo-col col-9">
						<div class="thumbnail">
							<div class="images"></div>
							<div class="content">
								<div class="left">
									<span class="date">
										<i class="far fa-calendar-alt"></i> Valid until Aug 18, 2019
									</span>	
									<h4 class="">Nike Factory Super Merdeka PROMO!</h4>
									
								</div>	
								<div class="right">
									<div class="circle-frame type-2">
										<i class="fas fa-percentage"></i>
									</div>
								</div>
							</div>
						</div>	
					</div>
					<div class="promo-col col-9">
						<div class="thumbnail">
							<div class="images"></div>
							<div class="content">
								<div class="left">
									<span class="date">
										<i class="far fa-calendar-alt"></i> Valid until Aug 18, 2019
									</span>	
									<h4 class="">Pringless, Discount up to 40%!</h4>
									
								</div>	
								<div class="right">
									<div class="circle-frame type-1">
										<i class="fas fa-percentage"></i>
									</div>
								</div>
							</div>
						</div>	
					</div>
					<div class="promo-col col-5">
						<div class="card view-more">
							<i class="fas fa-plus"></i><br>
							view more
						</div>	
					</div>
				</div>	
			</div> 
			<div class="divider-title">
				<i class="fas fa-ellipsis-h right"></i>
				<div class="title">
					<h4 class="display-5"><b>Top</b> Stores</h4>
				</div>
			</div>
			<div class="slider-frame">
				<div class="overflow-brand-sm row">
					<div class="brand-sm-col col-3">
						<div class="icon">
							<img src="asset/images/tenant-logo-1.png">
						</div>
						<div class="disc-info">30% Off</div>
					</div>
					<div class="brand-sm-col col-3">
						<div class="icon">
							<img src="asset/images/tenant-logo-2.png">
						</div>
						<div class="disc-info">20% Off</div>
					</div>
					<div class="brand-sm-col col-3">
						<div class="icon">
							<img src="asset/images/tenant-logo-3.png">
						</div>
						<div class="disc-info">70% Off</div>
					</div>
					<div class="brand-sm-col col-3">
						<div class="icon">
							<img src="asset/images/tenant-logo-4.png">
						</div>
						<div class="disc-info">50% Off</div>
					</div>
					<div class="brand-sm-col col-3">
						<div class="icon">
							<img src="asset/images/tenant-logo-5.png">
						</div>
						<div class="disc-info">32% Off</div>
					</div>
				</div>
			</div>
			<div class="divider-title">
				<i class="fas fa-ellipsis-h right"></i>
				<div class="title">
					<h4 class="display-5"><b>Top Malls</b> in the City!</h4>
				</div>
			</div>
			<div class="slider-frame">
				<div class="overflow-full-pict row nearby-index">
					<div class="full-pict-col col-6">
						<div class="full-pict">
							<div class="images"></div>
							<div class="content">
								<div class="title">
									<h4>Senayan City</h4>
									<span class="icon">
										<i class="fas fa-hand-holding-usd"></i> 2159 Promo
									</span>
								</div>
							</div>
						</div>	
					</div>
					<div class="full-pict-col col-6">
						<div class="full-pict">
							<div class="images"></div>
							<div class="content">
								<div class="title">
									<h4>Plaza Semanggi</h4>
									<span class="icon">
										<i class="fas fa-hand-holding-usd"></i> 659 Promo
									</span>
								</div>
							</div>
						</div>	
					</div>
					<div class="full-pict-col col-6">
						<div class="full-pict">
							<div class="images"></div>
							<div class="content">
								<div class="title">
									<h4>Grand Indonesia</h4>
									<span class="icon">
										<i class="fas fa-hand-holding-usd"></i> 418 Promo
									</span>
								</div>
							</div>
						</div>	
					</div>
					<div class="full-pict-col col-6">
						<div class="full-pict">
							<div class="images"></div>
							<div class="content">
								<div class="title">
									<h4>Plaza Indonesia</h4>
									<span class="icon">
										<i class="fas fa-hand-holding-usd"></i> 1019 Promo
									</span>
								</div>
							</div>
						</div>	
					</div>
					<div class="full-pict-col col-6">
						<div class="full-pict">
							<div class="images"></div>
							<div class="content">
								<div class="title">
									<h4>Ambassador</h4>
									<span class="icon">
										<i class="fas fa-hand-holding-usd"></i> 763 Promo
									</span>
								</div>
							</div>
						</div>	
					</div>
					<div class="full-pict-col col-6">
						<div class="full-pict">
							<div class="images"></div>
							<div class="content">
								<div class="title">
									<h4>Kuningan City</h4>
									<span class="icon">
										<i class="fas fa-hand-holding-usd"></i> 763 Promo
									</span>
								</div>
							</div>
						</div>	
					</div>
					<div class="promo-col col-5">
						<div class="card view-more">
							<i class="fas fa-plus"></i><br>
							view more
						</div>	
					</div>
				</div>	
			</div> <!-- p18 end -->
			<div class="footer-frame"></div>
		</div><!-- master body -->
	</body>

	<script>
		$(document).ready(function() {
			$('.right-frame').click(function() {
				$('.filter').addClass('active');
			});
			$('.button-close').click(function() {
				$('.filter').removeClass('active');
			});
		})
	</script>	
</html>

