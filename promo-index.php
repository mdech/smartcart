<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>SmartCart Mockup</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="shortcut icon" type="image" href="asset/images/gaspace-icon.png">
		<link rel="stylesheet" type="text/css" href="asset/css/promo-index.css" media="all"/>
		<link rel="stylesheet" type="text/css" href="asset/css/animation.css" media="all"/>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
		<script src="asset/js/jquery-2.1.1.js"></script>
		<script src="asset/js/jquery-ui.js"></script>
		<script src="asset/js/smartcart-master.js"></script>
		
	</head>
	<body>
		<div class="master-body">
			<div class="navbar-top">
				<div class="center-logo"></div>
				<div class="right-frame">
					<i class="fas fa-user"></i>
				</div>
			</div>
			<div class="navbar-bottom row">
				<div class="col-3 list" onclick="location.href='index.php';">
					<div class="icon"></div>
					<div class="label">
						Home
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list selected" onclick="location.href='promo-index.php';">
					<div class="icon"></div>
					<div class="label">
						Promo
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list" onclick="location.href='nearby-index.php';">
					<div class="icon"></div>
					<div class="label">
						Nearby
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list" onclick="location.href='credit-score.php';">
					<div class="icon"></div>
					<div class="label">
						Target
					</div>	
					<span class="clear"></span>
				</div>
			</div>

			<!-- S T A R T   C U S T O M I Z A T I O N   F R O M   H E R E -->

			<div class="header promo-index"> <!-- Header -->
				<div class="p-18">
					<!-- <div class="bottom-image"></div> -->
				</div>
			</div>

			<div class="p-18"> <!-- body -->	
				<div class="row promo-index-1">
					
				</div>
				
			</div>
			<div class="promo-index-2">
				<div class="col-12">
					<div class="card">
						<div class="p-18">
							<i class="fas fa-search left"></i>
							<input class="input-search" type="text" placeholder="Im interested in ..." required>
							<i class="fas fa-filter right"></i>
							<br class="clear">
						</div>
					</div>
				</div>
			</div>
			<br class="clear">
			<div class="divider-title">
				<i class="fas fa-ellipsis-h right"></i>
				<div class="title">
					<h5 class="display-5"><b>Favorite</b> Stores</h5>
				</div>
			</div>
			<div class="slider-frame">
				<div class="overflow-brand-sm row">
					<div class="brand-sm-col col-3">
						<div class="icon">
							<img src="asset/images/store-alfamart.jpeg">
						</div>
						<div class="disc-info">Alfamart</div>
					</div>
					<div class="brand-sm-col col-3">
						<div class="icon">
							<img src="asset/images/store-indomart.jpg">
						</div>
						<div class="disc-info">Indomart</div>
					</div>
					<div class="brand-sm-col col-3">
						<div class="icon">
							<img src="asset/images/store-carrefour.gif">
						</div>
						<div class="disc-info">Carrefour</div>
					</div>
					<div class="brand-sm-col col-3">
						<div class="icon">
							<img src="asset/images/store-giant.jpg">
						</div>
						<div class="disc-info">Giant</div>
					</div>
				</div>
			</div>
			<div class="divider-title" style="display: none;">
				<div class="title">
					<h5 class="display-5"><b>Browse</b> by Category</h5>
				</div>
			</div>
			<div class="slider-frame" style="display: none;">
				<div class="overflow-category row">
					<div class="category-col col-24">
						<div class="images"></div>
						<div class="title">Makanan</div>
					</div>
					<div class="category-col col-24">
						<div class="images"></div>
						<div class="title">Belanja</div>
					</div>
					<div class="category-col col-24">
						<div class="images"></div>
						<div class="title">Fashion</div>
					</div>
					<div class="category-col col-24">
						<div class="images"></div>
						<div class="title">Hiburan</div>
					</div>
					<div class="category-col col-24">
						<div class="images"></div>
						<div class="title">Film</div>
					</div>
					<div class="category-col col-24">
						<div class="images"></div>
						<div class="title">Olahraga</div>
					</div>
					<div class="category-col col-24">
						<div class="images"></div>
						<div class="title">Transportasi</div>
					</div>
				</div>
			</div>	
			<div class="divider-title">
				<i class="fas fa-ellipsis-h right"></i>
				<div class="title">
					<h5 class="display-5"><b>Today's Best</b> Offers</h5>
				</div>
			</div>
			<div class="slider-frame">
				<div class="overflow row">
					<div class="promo-col col-9">
						<div class="thumbnail">
							<div class="images"></div>
							<div class="content">
								<div class="left">
									<span class="date">
										<i class="far fa-calendar-alt"></i> Only TODAY !
									</span>	
									<h5 class="">Indomie goreng 65gr pck</h5>
									
								</div>	
								<div class="right">
									<div class="circle-frame type-1">
										<i class="fas fa-percentage"></i>
									</div>
								</div>
							</div>
						</div>	
					</div>
					<div class="promo-col col-9">
						<div class="thumbnail">
							<div class="images"></div>
							<div class="content">
								<div class="left">
									<span class="date">
										<i class="far fa-calendar-alt"></i> Valid until Mar 4, 2019
									</span>	
									<h5 class="">LAY'S Snack Potato Chips Rachlete</h5>
									
								</div>	
								<div class="right">
									<div class="circle-frame type-2">
										<i class="fas fa-percentage"></i>
									</div>
								</div>
							</div>
						</div>	
					</div>
					<div class="promo-col col-9">
						<div class="thumbnail">
							<div class="images"></div>
							<div class="content">
								<div class="left">
									<span class="date">
										<i class="far fa-calendar-alt"></i> Valid until Feb 18, 2019
									</span>	
									<h5 class="">Unilever Lebaran MEGA SALE !</h5>
									
								</div>	
								<!-- <div class="right">
									<div class="circle-frame type-1">
										<i class="fas fa-percentage"></i>
									</div>
								</div> -->
							</div>
						</div>	
					</div>
					<div class="promo-col col-9">
						<div class="thumbnail">
							<div class="images"></div>
							<div class="content">
								<div class="left">
									<span class="date">
										<i class="far fa-calendar-alt"></i> Valid until Apr 18, 2019
									</span>	
									<h5 class="">Listerine Zero Buy 1 GET 1</h5>
									
								</div>	
								<div class="right">
									<div class="circle-frame type-3">
										<i class="fas fa-barcode"></i>
									</div>
								</div>
							</div>
						</div>	
					</div>
					<div class="promo-col col-9">
						<div class="thumbnail">
							<div class="images"></div>
							<div class="content">
								<div class="left">
									<span class="date">
										<i class="far fa-calendar-alt"></i> Valid until Aug 18, 2019
									</span>	
									<h5 class="">Nike Factory Super Merdeka PROMO!</h5>
									
								</div>	
								<div class="right">
									<div class="circle-frame type-2">
										<i class="fas fa-percentage"></i>
									</div>
								</div>
							</div>
						</div>	
					</div>
					<div class="promo-col col-9">
						<div class="thumbnail">
							<div class="images"></div>
							<div class="content">
								<div class="left">
									<span class="date">
										<i class="far fa-calendar-alt"></i> Valid until Aug 18, 2019
									</span>	
									<h5 class="">Pringless, Discount up to 40%!</h5>
									
								</div>	
								<div class="right">
									<div class="circle-frame type-1">
										<i class="fas fa-percentage"></i>
									</div>
								</div>
							</div>
						</div>	
					</div>
					<div class="promo-col col-5">
						<div class="card view-more">
							<i class="fas fa-plus"></i><br>
							view more
						</div>	
					</div>
				</div>	
			</div> 
			<div class="divider-title">
				<i class="fas fa-ellipsis-h right"></i>
				<div class="title">
					<h5 class="display-5"><b>Browse</b> by Coupons</h5>
				</div>
			</div>
			<div class="slider-frame">
				<div class="overflow-coupon row">
					<div class="coupon-col col-9">
						<div class="coupon-frame card">
							<div class="padding">
								<div class="tenant-info">
									<div class="logo">
										<img src="asset/images/tenant-logo-1.png">
									</div>
									<div class="title">Bread life</div>
									<Br class="clear">
								</div>
								<div class="disc-info">
									<h4>50% OFF <br>Paket 6 Roti Bite Series</h4>
									<div>Berlaku hanya untuk pembelian dengan app untuk pertama kali</div>
								</div>
								<div class="footer">
									<div class="border-top"></div>
									<div class="row">
										<div class="col-4">
											<div class="icon">
												<i class="far fa-calendar-alt"></i>
											</div>
											<div class="text">
												Exp Date
												<div class="date">21 Feb 2018</div>	
											</div>	
										</div>
										<div class="col-8">
											<div class="icon">
												<i class="fas fa-award"></i>
											</div>	
											<div class="text">
												Used
												<div>102 times <br>(1230 remaining)</div>
											</div>	
										</div>	
									</div>	
								</div>
							</div>
						</div>
					</div>
					<div class="coupon-col col-9">
						<div class="coupon-frame card">
							<div class="padding">
								<div class="tenant-info">
									<div class="logo">
										<img src="asset/images/tenant-logo-3.png">
									</div>
									<div class="title">Dum Dum Thai Tea</div>
									<Br class="clear">
								</div>
								<div class="disc-info">
									<h4>Buy 1 Get 1 !</h4>
									<div>Khusus menggunakan GO-PAY</div>
								</div>
								<div class="footer">
									<div class="border-top"></div>
									<div class="row">
										<div class="col-4">
											<div class="icon">
												<i class="far fa-calendar-alt"></i>
											</div>
											<div class="text">
												Exp Date
												<div class="date">1 Des 2019</div>	
											</div>	
										</div>
										<div class="col-8">
											<div class="icon">
												<i class="fas fa-award"></i>
											</div>	
											<div class="text">
												Used
												<div>0 times <br>(2000 remaining)</div>
											</div>	
										</div>	
									</div>	
								</div>
							</div>
						</div>
					</div>
					<div class="coupon-col col-9">
						<div class="coupon-frame card">
							<div class="padding">
								<div class="tenant-info">
									<div class="logo">
										<img src="asset/images/tenant-logo-2.png">
									</div>
									<div class="title">D'crepes</div>
									<Br class="clear">
								</div>
								<div class="disc-info">
									<h4>Cashback 30%</h4>
									<div>Khusus pembayaran di tempat menggunakan GO-PAY</div>
								</div>
								<div class="footer">
									<div class="border-top"></div>
									<div class="row">
										<div class="col-4">
											<div class="icon">
												<i class="far fa-calendar-alt"></i>
											</div>
											<div class="text">
												Exp Date
												<div class="date">7 Mar 2018</div>	
											</div>	
										</div>
										<div class="col-8">
											<div class="icon">
												<i class="fas fa-award"></i>
											</div>	
											<div class="text">
												Used
												<div>18 times <br>(90 remaining)</div>
											</div>	
										</div>	
									</div>	
								</div>
							</div>
						</div>
					</div>
					<div class="coupon-col col-9">
						<div class="coupon-frame card">
							<div class="padding">
								<div class="tenant-info">
									<div class="logo">
										<img src="asset/images/tenant-logo-5.png">
									</div>
									<div class="title">Chatime</div>
									<Br class="clear">
								</div>
								<div class="disc-info">
									<h4>Discount up to 70%</h4>
									<div>Hanya untuk menu baru terpilih</div>
								</div>
								<div class="footer">
									<div class="border-top"></div>
									<div class="row">
										<div class="col-4">
											<div class="icon">
												<i class="far fa-calendar-alt"></i>
											</div>
											<div class="text">
												Exp Date
												<div class="date">12 Jun 2019</div>	
											</div>	
										</div>
										<div class="col-8">
											<div class="icon">
												<i class="fas fa-award"></i>
											</div>	
											<div class="text">
												Used
												<div>12 times <br>(15 remaining)</div>
											</div>	
										</div>	
									</div>	
								</div>
							</div>
						</div>
					</div>
					<div class="coupon-col col-9">
						<div class="coupon-frame card">
							<div class="padding">
								<div class="tenant-info">
									<div class="logo">
										<img src="asset/images/tenant-logo-1.png">
									</div>
									<div class="title">Shihlin Taiwan snacks</div>
									<Br class="clear">
								</div>
								<div class="disc-info">
									<h4>Discount up to 70%</h4>
									<div>Hanya untuk menu baru terpilih</div>
								</div>
								<div class="footer">
									<div class="border-top"></div>
									<div class="row">
										<div class="col-4">
											<div class="icon">
												<i class="far fa-calendar-alt"></i>
											</div>
											<div class="text">
												Exp Date
												<div class="date">12 Jun 2019</div>	
											</div>	
										</div>
										<div class="col-8">
											<div class="icon">
												<i class="fas fa-award"></i>
											</div>	
											<div class="text">
												Used
												<div>12 times <br>(15 remaining)</div>
											</div>	
										</div>	
									</div>	
								</div>
							</div>
						</div>
					</div>	
				</div>
			</div>	

			<div class="footer-frame"></div>
		</div><!-- master body -->
	</body>

	<script>
		$(document).ready(function() {
			$('.right-frame').click(function() {
				$('.filter').addClass('active');
			});
			$('.button-close').click(function() {
				$('.filter').removeClass('active');
			});
		})
	</script>	
</html>

