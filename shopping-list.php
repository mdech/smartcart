<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>SmartCart Mockup</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="shortcut icon" type="image" href="asset/images/gaspace-icon.png">
		<link rel="stylesheet" type="text/css" href="asset/css/shopping-list.css" media="all"/>
		<link rel="stylesheet" type="text/css" href="asset/css/animation.css" media="all"/>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
		<script src="asset/js/jquery-2.1.1.js"></script>
		<script src="asset/js/jquery-ui.js"></script>
		<script src="asset/js/smartcart-master.js"></script>
		
	</head>
	<body>
		<div class="master-body">
			<div class="navbar-top">
				<div class="center-logo"></div>
				<div class="right-frame">
					<i class="fas fa-user"></i>
				</div>
			</div>
			<div class="navbar-bottom row">
				<div class="col-3 list" onclick="location.href='index.php';">
					<div class="icon"></div>
					<div class="label">
						Home
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list" onclick="location.href='promo-index.php';">
					<div class="icon"></div>
					<div class="label">
						Promo
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list" onclick="location.href='nearby-index.php';">
					<div class="icon"></div>
					<div class="label">
						Nearby
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list" onclick="location.href='credit-score.php';">
					<div class="icon"></div>
					<div class="label">
						Target
					</div>	
					<span class="clear"></span>
				</div>
			</div>

			<!-- S T A R T   C U S T O M I Z A T I O N   F R O M   H E R E -->

			<div class="header shopping-list index"> <!-- Header -->
				<div class="p-18">
					<!-- <div class="bottom-image"></div> -->
				</div>
			</div>
			<div class="p-18">
				<div class="row first-row">
					<div class="col-12">
						<div class="card">
							<div class="p-10 center">
								<h5 class="display-5"><b>Shopping</b> List</h5>
							</div>
						</div>
					</div>
				</div>	
				<div class="row">
					<div class="sl-sm-col col-12">
						<div class="card">
							<div class="p-10">
								<div class="image">
									<img src="asset/images/sl-image-popmie.jpg">
								</div>
								<div class="content">
									<div class="name">POP MIE Baso Special Jumbo 75gr</div>
									<div class="price"><span>Rp</span> 6,000</div>
									<div class="add-item-box">
										<div class="icon">
											<i class="fas fa-minus"></i>
										</div>
										<div class="center">2</div>
										<div class="icon">
											<i class="fas fa-plus"></i>
										</div>
									</div>
									<br class="clear">
								</div>
								<br class="clear">
							</div>
						</div>

						<div class="card">
							<div class="p-10">
								<div class="image">
									<img src="asset/images/sl-image-bayfresh.jpg">
								</div>
								<div class="content">
									<div class="name">BAYFRESH Pop Scent Lovely 10gr</div>
									<div class="price"><span>Rp</span> 9,000</div>
									<div class="add-item-box">
										<div class="icon">
											<i class="fas fa-minus"></i>
										</div>
										<div class="center">2</div>
										<div class="icon">
											<i class="fas fa-plus"></i>
										</div>
									</div>
									<br class="clear">
								</div>
								<br class="clear">
							</div>
						</div>

						<div class="card">
							<div class="p-10">
								<div class="image">
									<img src="asset/images/sl-image-gowell.jpg">
								</div>
								<div class="content">
									<div class="name">GOWELL Rasa Taro 5x29gr</div>
									<div class="price"><span>Rp</span> 6,500</div>
									<div class="add-item-box">
										<div class="icon">
											<i class="fas fa-minus"></i>
										</div>
										<div class="center">3</div>
										<div class="icon">
											<i class="fas fa-plus"></i>
										</div>
									</div>
									<br class="clear">
								</div>
								<br class="clear">
							</div>
						</div>

						<div class="card">
							<div class="p-10">
								<div class="image">
									<img src="asset/images/sl-image-indomie.jpg">
								</div>
								<div class="content">
									<div class="name">INDOMIE New Kari 72gr</div>
									<div class="price"><span>Rp</span> 3,000</div>
									<div class="add-item-box">
										<div class="icon">
											<i class="fas fa-minus"></i>
										</div>
										<div class="center">12</div>
										<div class="icon">
											<i class="fas fa-plus"></i>
										</div>
									</div>
									<br class="clear">
								</div>
								<br class="clear">
							</div>
						</div>
					
						<div class="card">
							<div class="p-10">
								<div class="image">
									<img src="asset/images/sl-image-ovomaltine.jpg">
								</div>
								<div class="content">
									<div class="name">OVOMALTINE Crunchy Cream 380gr (BPOM)</div>
									<div class="price"><span>Rp</span> 72,000</div>
									<div class="add-item-box">
										<div class="icon">
											<i class="fas fa-minus"></i>
										</div>
										<div class="center">1</div>
										<div class="icon">
											<i class="fas fa-plus"></i>
										</div>
									</div>
									<br class="clear">
								</div>
								<br class="clear">
							</div>
						</div>
					
						<div class="card">
							<div class="p-10">
								<div class="image">
									<img src="asset/images/sl-image-bango.png">
								</div>
								<div class="content">
									<div class="name">BANGO Kecap Manis Pouch 575ml</div>
									<div class="price"><span>Rp</span> 23,500</div>
									<div class="add-item-box">
										<div class="icon">
											<i class="fas fa-minus"></i>
										</div>
										<div class="center">1</div>
										<div class="icon">
											<i class="fas fa-plus"></i>
										</div>
									</div>
									<br class="clear">
								</div>
								<br class="clear">
							</div>
						</div>
					
						<div class="card">
							<div class="p-10">
								<div class="image">
									<img src="asset/images/sl-image-potabee.jpg">
								</div>
								<div class="content">
									<div class="name">POTABEE Regular Beef BBQ 68g</div>
									<div class="price"><span>Rp</span> 8,000</div>
									<div class="add-item-box">
										<div class="icon">
											<i class="fas fa-minus"></i>
										</div>
										<div class="center">5</div>
										<div class="icon">
											<i class="fas fa-plus"></i>
										</div>
									</div>
									<br class="clear">
								</div>
								<br class="clear">
							</div>
						</div>
					
						<div class="card">
							<div class="p-10">
								<div class="image">
									<img src="asset/images/sl-image-rosebrand.jpg">
								</div>
								<div class="content">
									<div class="name">ROSE BRAND Gula Kristal Premium 1 kg</div>
									<div class="price"><span>Rp</span> 12,000</div>
									<div class="add-item-box">
										<div class="icon">
											<i class="fas fa-minus"></i>
										</div>
										<div class="center">4</div>
										<div class="icon">
											<i class="fas fa-plus"></i>
										</div>
									</div>
									<br class="clear">
								</div>
								<br class="clear">
							</div>
						</div>
					
						<div class="card">
							<div class="p-10">
								<div class="image">
									<img src="asset/images/sl-image-sariwangi.jpg">
								</div>
								<div class="content">
									<div class="name">SARIWANGI Teh Asli 50 The Celup</div>
									<div class="price"><span>Rp</span> 10,500</div>
									<div class="add-item-box">
										<div class="icon">
											<i class="fas fa-minus"></i>
										</div>
										<div class="center">3</div>
										<div class="icon">
											<i class="fas fa-plus"></i>
										</div>
									</div>
									<br class="clear">
								</div>
								<br class="clear">
							</div>
						</div>
					
						<div class="card">
							<div class="p-10">
								<div class="image">
									<img src="asset/images/sl-image-kapalapi.jpg">
								</div>
								<div class="content">
									<div class="name">Kapal Api Gula Rasa Mantap Bag 25gr x 30 pcs</div>
									<div class="price"><span>Rp</span> 32,000</div>
									<div class="add-item-box">
										<div class="icon">
											<i class="fas fa-minus"></i>
										</div>
										<div class="center">1</div>
										<div class="icon">
											<i class="fas fa-plus"></i>
										</div>
									</div>
									<br class="clear">
								</div>
								<br class="clear">
							</div>
						</div>
					
						<div class="card">
							<div class="p-10">
								<div class="image">
									<img src="asset/images/sl-image-sunlight.jpg">
								</div>
								<div class="content">
									<div class="name">SUNLIGHT Lime Refill 780ml</div>
									<div class="price"><span>Rp</span> 15,000</div>
									<div class="add-item-box">
										<div class="icon">
											<i class="fas fa-minus"></i>
										</div>
										<div class="center">3</div>
										<div class="icon">
											<i class="fas fa-plus"></i>
										</div>
									</div>
									<br class="clear">
								</div>
								<br class="clear">
							</div>
						</div>
						<br class="clear">
					</div>
				</div>
			</div>	
			<div class="footer-frame"></div>
		</div><!-- master body -->
	</body>
</html>

