<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>SmartCart Mockup</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="shortcut icon" type="image" href="asset/images/gaspace-icon.png">
		<link rel="stylesheet" type="text/css" href="asset/css/stockbit-trade.css" media="all"/>
		<link rel="stylesheet" type="text/css" href="asset/css/animation.css" media="all"/>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
		<script src="asset/js/jquery-2.1.1.js"></script>
		<script src="asset/js/jquery-ui.js"></script>
		<script src="asset/js/smartcart-master.js"></script>
		
	</head>
	<body>
		<div class="master-body">
			<div class="navbar-top">
				<div class="center-logo"></div>
				<div class="right-frame">
					<i class="fas fa-user"></i>
				</div>
			</div>
			<div class="navbar-bottom row">
				<div class="col-3 list" onclick="location.href='index.php';">
					<div class="icon"></div>
					<div class="label">
						Home
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list" onclick="location.href='promo-index.php';">
					<div class="icon"></div>
					<div class="label">
						Promo
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list" onclick="location.href='nearby-index.php';">
					<div class="icon"></div>
					<div class="label">
						Nearby
					</div>	
					<span class="clear"></span>
				</div>
				<div class="col-3 list" onclick="location.href='credit-score.php';">
					<div class="icon"></div>
					<div class="label">
						Target
					</div>	
					<span class="clear"></span>
				</div>
			</div>

			<!-- S T A R T   C U S T O M I Z A T I O N   F R O M   H E R E -->

			<div class="header loyalty"> <!-- Header -->
				<div class="p-18">
					<!-- <div class="bottom-image"></div> -->
				</div>
			</div>
			<div class="p-18">
				<div class="row first-row">
					<div class="col-12">
						<div class="card">
							<div class="p-10 center">
								<h4 class="display-4"><b>My Rewards</b> Room</h4>
							</div>
						</div>
					</div>
				</div>
				<div class="loyalty-detail-2">
					<div class="row pull-row">
						<div class="col-6 coupon-tab active">	
							<div class="card">
								<img src="asset/images/logo-bibit-long.png" alt="" style="height:20px; margin-top: 10px">	
							</div>
						</div>
						<div class="col-6 exclusive-tab">	
							<div class="card">
								<img src="asset/images/logo-e-emas.png" alt="" style="height:35px; margin-top: 5px">
							</div>
						</div>
					</div>
					
					<div class="tab-frame">
						<div class="slide">

							<div class="tab coupon active">
								<div class="trades-wrapper">
									<div class="p-18">
										<img src="asset/images/ss-trade-1.png" alt="" class="my-trade-ss">
									</div>
								</div>	
							</div>	

							<div class="tab exclusive">

								<div class="trades-wrapper">
									<img src="asset/images/stockbit-2.jpg" alt="" class="my-trade-ss">
								</div>
							</div>

							<br class="clear">
						</div>	
					</div>			
				</div>
			</div>	
			<div class="footer-frame"></div>
		</div><!-- master body -->
	</body>
</html>

